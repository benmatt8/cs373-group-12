# cs373-group-12

**Canvas Group Number**: 
12

**Names of the team members:**
Arushi Sharma, Anvi Bajpai, Nikhita Meka, Ben Matthews, Jorge Martinez

**Name of the project:** 
SafEats4You (safeats4you.lol) 

**Gitlab SHA (most recent)** 
88df4e16c317e429ad90be5ade17787caff9c3a5
 
**Link to Pecha Kucha**
https://youtu.be/I96ZTBqviXc?si=KCnHoA8_csBxTAuC

**API Docs Link** 
https://documenter.getpostman.com/view/31524688/2sA2r545M5

**Phase 1 Leader** 
Arushi Sharma-managed the first phase as one of the front-end developers by making sure tasks were split up to finish on time. 

**Phase 2 Leader** 
Ben Matthews managed the second phase as one of the back-end developers by making sure tasks were split up to finish on time. 

**Phase 3 Leader** 
Anvi Bajpai managed the third phase as one of the back-end developers by making sure tasks were split up to finish on time. 

**Phase 4 Leader** 
Nikhita Meka managed the fourth phase as one of the front-end developers by making sure tasks were split up to finish on time. 

**Link to GitLab pipelines** 
https://gitlab.com/arushisharma/cs373-group-12/-/pipelines

**Link to Website** 
https://www.safeats4you.lol/home

**Link to API** 
https://platform.safeats4you.lol/

**The proposed project:** 
In the face of the food allergy epidemic, it's more crucial than ever to be aware and careful. Our mission is to empower individuals with dietary restrictions (Vegetarian, Vegan, Pescatarian, Nut-Free, Celiac, Lactose, Soy Allergy, Shellfish, Sesame), ensuring they can add variety to their diet without compromising safety. We're dedicated to raising awareness about allergies, offering a curated platform for discovering safe, certified places to eat, and providing personalized recipes and meal plans. Our goal is to enable people to confidently enjoy diverse culinary experiences, navigating dining choices with ease and joy while prioritizing their well-being. Through our project, we aim to build a community where dietary restrictions enhance, rather than limit, lifestyle choices.

**URLs of at least three data sources that you will programmatically scrape:**
1. Restful API: https://docs.developer.yelp.com/docs/fusion-intro (Yelp Fusion API) 
2. https://developers.google.com/youtube/v3 (Youtube API) 
3. https://developers.google.com/maps (Google Maps API)
4. https://www.webmd.com/diet/sesame-oil-good-for-you (WebMD) 
5. https://tasty.co/ (Tasty Website) 

**Models:**
1. Different Types of Allergies & Dietary Restrictions
2. Restaurants in Austin 
3. Recipes and Meal Plans 

**An estimate of the number of instances of each model**
 5-8 instances per model

**Attributes of Each Model:** 
1. Different Types of Allergies & Dietary Restrictions
    - Alphabetical order 
    - Severity
    - Prevalence
    - Allergy
    - Lifestyle choice 
2. Restaurants in Austin 
    - Distance 
    - Alphabetical order 
    - Cuisine 
    - Cost 
    - Specific Allergy 
    - Rating (Health Score)
3. Recipes and Meal Plans 
    - Alphabetical order 
    - Cuisine 
    - Cost 
    - Specific Allergy  
    - Media Type 

**Instances of each model must connect to instances of at least two other models:**
Restrictions will lead to restaurants or to recipes/meal plans that are suitable choices for people with those restrictions.

**Describe two types of media for instances of each model:**
1. Different Types of Allergies & Dietary Restrictions
    - WebMD 
    - Videos expanding on the allergy
2. Restaurants in Austin 
    - Maps
    - Images 
    - Menus
3. Recipes and Meal Plans 
    - Tasty feed 
    - Videos of recipes 

**Describe three questions that your site will answer:**
1. What are places that are safe to eat? 
2. What kinds of recipes can you make that ensure a diverse meal plan? 
3. What are different types of allergies, how to diagnose, and symptoms to watch out for?

**Phase 1 time to completion (estimated):**
20 hours (Arushi), 20 hours (Anvi), 20 hours (Nikhita), 20 hours (Ben), 20 hours (Jorge)                     
**Phase 1 time to completion (actual):**
 22 hours (Arushi), 22 hours (Anvi), 22 hours (Nikhita), 18 hours (Ben), 12 hours (Jorge)

 **Phase 2 time to completion (estimated):**
20 hours (Arushi), 20 hours (Anvi), 20 hours (Nikhita), 20 hours (Ben), 20 hours (Jorge)                     
**Phase 2 time to completion (actual):**
 22 hours (Arushi), 22 hours (Anvi), 22 hours (Nikhita), 22 hours (Ben), 22 hours (Jorge)

**Phase 3 time to completion (estimated):**
20 hours (Arushi), 20 hours (Anvi), 20 hours (Nikhita), 20 hours (Ben), 20 hours (Jorge)                     
**Phase 3 time to completion (actual):**
 20 hours (Arushi), 20 hours (Anvi), 20 hours (Nikhita), 20 hours (Ben), 20 hours (Jorge)

**Phase 4 time to completion (estimated):**
15 hours (Arushi), 12 hours (Anvi), 15 hours (Nikhita), 10 hours (Ben), 10 hours (Jorge)                     
**Phase 4 time to completion (actual):**
14 hours (Arushi), 12 hours (Anvi), 14 hours (Nikhita), 12 hours (Ben), 12 hours (Jorge)



 Run 
----
1. python3 -m venv venv
2. Linux: source venv/bin/activate or Windows: venv\Scripts\activate 
3. pip3 install -r requirements.txt
4. python3 app.py

 **Comments: **
 In app.py, we credit Southwest Hurricane Aid's repository and code from the Fall 2023 semester for our endpoints and the approach for searching, filtering, and sorting.
