import mysql.connector
import json
import requests

# AWS Creds
safe_eats_host = "safeeats-db.c1sq0s4ecml1.us-west-1.rds.amazonaws.com"
safe_eats_username = "user"
safe_eats_pass = "SafeEats123$"
safe_eats_db_name = "models"
safe_eats_port = "3307"

def main():
    # insert_restaurant_data()
    # insert_recipe_data()
    # insert_allergy_data()
    pass

def insert_recipe_data():

    print("Connecting...")

    # Database connection
    safe_eats_db = mysql.connector.connect(
        host = safe_eats_host,
        user = safe_eats_username,
        password = safe_eats_pass,
        database = safe_eats_db_name,
        port = safe_eats_port
    )

    print("Connected")

    # read json data from a file
    with open("recipes.json", "r") as json_file:
        recipe_data = json.load(json_file)

    cursor = safe_eats_db.cursor()
    # Define the SQL query to insert data
    insert_query = """
    INSERT INTO recipes (videoId, title, channelTitle, description, publishTime, likeCount, viewCount, commentCount, imageUrl)
    VALUES (%(videoId)s, %(title)s, %(channelTitle)s, %(description)s, %(publishTime)s, %(likeCount)s, %(viewCount)s, %(commentCount)s, %(imageUrl)s)
    """

    for entry in recipe_data.get("items"):
        extracted_data = {
            "videoId": entry["id"],
            "title": entry["snippet"]["title"],
            "channelTitle": entry["snippet"]["channelTitle"],
            "description": entry["snippet"]["description"],
            "publishTime": entry["snippet"]["publishedAt"],
            "likeCount": entry["statistics"]["likeCount"] if "likeCount" in entry["statistics"] else "0",
            "viewCount": entry["statistics"]["viewCount"] if "viewCount" in entry["statistics"] else "0",
            "commentCount": entry["statistics"]["commentCount"] if "commentCount" in entry["statistics"] else "0",
            "imageUrl": entry["snippet"]["thumbnails"]["high"]["url"]
        }
        cursor.execute(insert_query, extracted_data)

    safe_eats_db.commit()
    print('Data inserted successfully') 

def insert_restaurant_data():
    print("Connecting...")

    # Database connection
    safe_eats_db = mysql.connector.connect(
        host = safe_eats_host,
        user = safe_eats_username,
        password = safe_eats_pass,
        database = safe_eats_db_name,
        port = safe_eats_port
    )

    print("Connected")

    # read json data from a file
    with open("restaurants.json", "r") as json_file:
        restaurant_data = json.load(json_file)

    cursor = safe_eats_db.cursor()
    # Define the SQL query to insert data
    insert_query = """
    INSERT INTO restaurants (name, display_address, distance, cuisine, rating, allergens, price, image_url, display_phone, is_closed, latitude, longitude, reviews, opening_hours)
    VALUES (%(name)s, %(display_address)s, %(distance)s, %(cuisine)s, %(rating)s, %(allergens)s, %(price)s, %(image_url)s, %(display_phone)s, %(is_closed)s, %(latitude)s, %(longitude)s, %(reviews)s, %(opening_hours)s)
    """

    for entry in restaurant_data.get("restaurants"):
        # Convert address list to JSON string
        display_address_str = ', '.join(entry["location"]["display_address"]) if entry.get("location") and entry["location"].get("display_address") else None

         # Convert reviews and opening_hours to JSON strings
        reviews_json = json.dumps(entry.get("reviews", []))
        opening_hours_json = json.dumps(entry.get("openingHours", []))

        extracted_data = {
            "name": entry["name"],
            "display_address": display_address_str,
            "distance": entry["distance"],
            "cuisine": entry["categories"][0]["title"] if entry.get("categories") else None,
            "rating": entry["rating"],
            "allergens": entry["allergens"] if entry.get("allergens") else None,
            "price": entry.get("price"),
            "image_url": entry.get("image_url"),
            "display_phone": entry.get("display_phone"),
            "is_closed": entry["is_closed"],
            "latitude": entry["coordinates"]["latitude"],
            "longitude": entry["coordinates"]["longitude"],
            "reviews": reviews_json,
            "opening_hours": opening_hours_json
        }
        cursor.execute(insert_query, extracted_data)

    safe_eats_db.commit()
    print('Data inserted successfully') 


def insert_allergy_data():
    print("Connecting...")

    # Database connection
    safe_eats_db = mysql.connector.connect(
        host = safe_eats_host,
        user = safe_eats_username,
        password = safe_eats_pass,
        database = safe_eats_db_name,
        port = safe_eats_port
    )

    print("Connected")

    # read json data from a file
    with open("allergy.json", "r") as json_file:
        allergy_data = json.load(json_file)

    cursor = safe_eats_db.cursor()
    # Define the SQL query to insert data
    insert_query = """
    INSERT INTO allergies (title, severity, prevalence, allergy, lifestyle, imageUrl, description, link)
    VALUES (%(title)s, %(severity)s, %(prevalence)s, %(allergy)s, %(lifestyle)s, %(imageUrl)s, %(description)s, %(link)s)
    """
    # UPDATING COLUMNS EXAMPLE
    # sql = '''
    #     UPDATE allergies SET commonAgeGroup = '{c}', triggers = '{tr}' WHERE title = '{t}'
    #     '''.format(c=commonGroup, t=title, tr=triggers)

    for entry in allergy_data.get("allergies"):
        extracted_data = {
            "title": entry["title"],
            "severity": entry["severity"],
            "prevalence": entry["prevalence"],
            "allergy": entry["allergy"],
            "lifestyle": entry["lifestyle"],
            "imageUrl": entry.get("imageUrl"),
            "description": entry.get("description"),
            "link": entry.get("link"),
        }
        cursor.execute(insert_query, extracted_data)

    safe_eats_db.commit()
    print('Data inserted successfully') 

if __name__ == "__main__":
  main()
