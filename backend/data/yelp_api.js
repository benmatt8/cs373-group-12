const yelp = require('yelp-fusion');
const fs = require('fs');

const apiKey = 'qyBasyAIYIVCHs0LqiajRzBB6GcpiiGy8H0108PUdqOPQ3jnrw60dZXNGfsp1ucPWkwA3QpMWIOZzrjsmpnbhcwcMdHEmhSyr510YhomAHIOwrjnSx6qYZdDb1nhZXYx';

const searchRequests = [
  { term: 'Vegan Diet', location: 'dallas, tx', limit: 4 },
  { term: 'Tree nuts Allergy', location: 'dallas, tx', limit: 4 },
];

const client = yelp.client(apiKey);

const fetchRestaurantData = async (searchRequest) => {
  const response = await client.search(searchRequest);
  const restaurants = response.jsonBody.businesses;
  const restaurantData = [];

  for (const restaurant of restaurants) {
    try {
      const detailsResponse = await client.business(restaurant.id);
      const details = detailsResponse.jsonBody;

      // Check if hours of operation exist and is not empty
      const openingHours = details.hours && details.hours[0]?.open ?
        details.hours[0].open.map(day => ({
          day: mapDayOfWeek(day.day),
          start: convertToRegularTime(day.start),
          end: convertToRegularTime(day.end)
        })) : [];
      restaurant.openingHours = openingHours;

      // Converting distance from meters to miles
      restaurant.distance = convertMetersToMiles(restaurant.distance);

      // Fetching reviews
      const reviewsResponse = await client.reviews(restaurant.alias);
      const reviews = reviewsResponse.jsonBody.reviews.map(review => ({
        text: review.text,
        rating: review.rating,
        reviewer: review.user.name
      }));
      restaurant.reviews = reviews;
      restaurant.allergens = searchRequest.term;
    } catch (error) {
      console.error(`Error fetching details for ${restaurant.name}:`, error);
      restaurant.openingHours = [];
      restaurant.reviews = [];
    }
    restaurantData.push(restaurant);
  }

  return restaurantData;
};

const allRestaurantData = [];

Promise.all(searchRequests.map(fetchRestaurantData))
  .then(results => {
    results.forEach(data => allRestaurantData.push(...data));
    const prettyJson = JSON.stringify(allRestaurantData, null, 4);
    fs.writeFileSync('restaurants.json', prettyJson, 'utf8');
    console.log('Restaurant data with converted distance, hours, and day names has been written to restaurants.json.');
  })
  .catch(e => {
    console.error('Error fetching restaurant data:', e);
  });

// Function to convert meters to miles
function convertMetersToMiles(meters) {
  return (meters * 0.000621371).toFixed(2);
}

// Function to convert military time to regular time with AM and PM
function convertToRegularTime(time) {
  let hour = parseInt(time.slice(0, 2));
  let minute = time.slice(2);
  let period = 'AM';
  if (hour >= 12) {
    period = 'PM';
    if (hour > 12) {
      hour -= 12;
    }
  }
  if (hour === 0) {
    hour = 12;
  }
  return `${hour}:${minute} ${period}`;
}

// Function to map numeric day values to actual day names
function mapDayOfWeek(day) {
  const daysOfWeek = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];
  return daysOfWeek[day];
}
