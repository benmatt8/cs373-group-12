import json
import requests

YOUTUBE_API_KEY = "AIzaSyCVgJJSk6KnRljT9sTIhcwodxKpkbZFDGM"

def get_youtube_ids(search, max_results):
    search = search.replace(" ", "%")
    print(search)
    ids = []
    url = f"https://youtube.googleapis.com/youtube/v3/search?part=snippet&maxResults={max_results}&q={search}&key={YOUTUBE_API_KEY}"
    response = requests.get(url)
    if response.status_code == 200:
        recipe_data = response.json()
        for entry in recipe_data["items"]:
            if "channelId" not in entry["id"]:
                ids.append(entry["id"]["videoId"])
        print ("Done: get_youtube_ids")
    else: 
        print("Failed: get_youtube_ids")
    return ids

def get_youtube_data(ids):
    if not ids: # is empty
        return
    p = iter(ids)
    ids_list = next(p)
    for id in p:
        addition = f"%2C{id}"
        ids_list += addition

    url = f"https://youtube.googleapis.com/youtube/v3/videos?part=snippet%2Cstatistics&id={ids_list}&key={YOUTUBE_API_KEY}"
    response = requests.get(url)

    if response.status_code == 200:
        recipe_data = response.json()
        with open("recipes.json", "w") as json_file:
            json.dump(recipe_data, json_file)
        print ("Done: get_youtube_data")
    else:
        print("Failed: get_youtube_ids")

if __name__ == "__main__":
    search = "Gluten Free Recipes"
    max_results = 25
    ids = get_youtube_ids(search, max_results)
    get_youtube_data(ids)