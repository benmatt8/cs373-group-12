from flask import Flask, Response, jsonify, request
from models import db, app, Allergy, Restaurant, Recipe
from schema import allergy_schema, restaurant_schema, recipe_schema
from sqlalchemy import or_
from sqlalchemy import and_
from sqlalchemy import func
import random

# NOTE: Example url http://127.0.0.1:5000/search/recipes/vegetarian?page=1
@app.route("/search/<string:model>/<string:search>", methods=['GET'])
def search_models(model, search):
    model = model.strip().lower()
    terms = search.split()
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=6)
    if model == "recipes":
        total = get_total_search_model(Recipe, terms)
        freq = search_model(Recipe, terms, page, per_page)

        keys = sorted(freq.keys(), key = lambda x: freq[x], reverse = True)
        recipes = recipe_schema.dump(keys, many=True)
        return jsonify({"recipes" : recipes,
                    "meta": {
                        "count": total
                    }})
    elif model == "restaurants":
        total = get_total_search_model(Restaurant, terms)
        freq = search_model(Restaurant, terms, page, per_page)

        keys = sorted(freq.keys(), key = lambda x: freq[x], reverse = True)
        restaurants = restaurant_schema.dump(keys, many=True)
        return jsonify({"restaurants" : restaurants,
                    "meta": {
                        "count": total
                    }})
    elif model == "allergyinfo":
        total = get_total_search_model(Allergy, terms)
        freq = search_model(Allergy, terms, page, per_page)

        keys = sorted(freq.keys(), key = lambda x: freq[x], reverse = True)
        allergies = allergy_schema.dump(keys, many=True)
        return jsonify({"allergyInfo" : allergies,
                    "meta": {
                        "count": total
                    }})

def search_model(model, terms, page, per_page):
    freq = {}
    print(type(model))
    for term in terms:
        queries = []
        if (model is Recipe):
            queries.append(model.videoId.contains(term))
            queries.append(model.title.contains(term))
            queries.append(model.channelTitle.contains(term))
            queries.append(model.description.contains(term))
        elif (model is Restaurant):
            queries.append(model.name.contains(term))
            queries.append(model.display_address.contains(term))
            queries.append(model.cuisine.contains(term))
            queries.append(model.allergens.contains(term))
        elif (model is Allergy):
            queries.append(model.title.contains(term))
            queries.append(model.description.contains(term))
        result = model.query.filter(or_(*queries)).paginate(page=page, per_page=per_page, error_out=False)
        for entry in result:
            if entry not in freq:
                freq[entry] = 1
            else:
                freq[entry] += 1
    return freq

def get_total_search_model(model, terms):
    freq = {}
    for term in terms:
        queries = []
        if (model is Recipe):
            queries.append(model.videoId.contains(term))
            queries.append(model.title.contains(term))
            queries.append(model.channelTitle.contains(term))
            queries.append(model.description.contains(term))
        elif (model is Restaurant):
            queries.append(model.name.contains(term))
            queries.append(model.display_address.contains(term))
            queries.append(model.cuisine.contains(term))
            queries.append(model.allergens.contains(term))
        elif (model is Allergy):
            queries.append(model.title.contains(term))
            queries.append(model.description.contains(term))
        result = model.query.filter(or_(*queries))
        for entry in result:
            if entry not in freq:
                freq[entry] = 1
            else:
                freq[entry] += 1
    return len(freq)

@app.route('/')
def home():
    return "SafeEats4You API is Working!!!!"

#GET RECIPES
@app.route('/recipes',  methods=['GET'])
def get_recipes():
    query = db.session.query(Recipe)
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=6)

    #cuisineFilter = request.args.get('cuisine', default='')
    allergyFilter = request.args.get('allergy', default='')

    # if cuisineFilter != '':
    #     query = query.filter(Recipe.cuisine.like("%" + cuisineFilter + "%"))
    if allergyFilter != '':
        query = query.join(Allergy, Recipe.allergyId == Allergy.id)\
                     .filter(Allergy.title == allergyFilter)

    sort = request.args.get('sortBy', default='')
    order = request.args.get('order', default='')
    sort_options = {
        'title': Recipe.title,
        'viewCount': Recipe.viewCount,
        'likeCount': Recipe.likeCount,
        'commentCount': Recipe.commentCount,
    }

    if sort in sort_options:
        if order == 'low':
            query = query.order_by(sort_options[sort])
        elif order == 'high':
            query = query.order_by(sort_options[sort].desc())


    num = query.paginate(page=page, per_page=per_page, error_out=False)
    schema = recipe_schema.dump(num, many=True)
 
    num = query.count()
    return jsonify({"recipes" : schema,
                    "meta": {
                        "count": num
                    }})

#INSTANCE RECIPE
@app.route('/recipes/<string:videoId>',  methods=['GET'])
def get_recipe_by_id(videoId):
    recipe = db.session.query(Recipe).filter_by(videoId=videoId).first()
    recipe_data = recipe_schema.dump(recipe)
    if recipe != None:
        return recipe_data
    else: 
        status = ("Recipe with videoId {videoId} not found.\n").format(videoId=videoId)
        return Response(status, status=404)

#GET RESTAURANTS
@app.route('/restaurants',  methods=['GET'])
def get_restaurants():

    query = db.session.query(Restaurant)
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=6)

    cuisineFilter = request.args.get('cuisine', default='')
    allergyFilter = request.args.get('allergy', default='')
    ratingFilter = request.args.get('rating', type=int, default='')
    distanceFilter = request.args.get('distance', type=int, default='')

    if allergyFilter != '':
        query = query.filter(Restaurant.allergens.like("%" + allergyFilter + "%"))
    if cuisineFilter != '':
        query = query.filter(Restaurant.cuisine.like("%" + cuisineFilter + "%"))
    if ratingFilter != '':
        query = query.filter(Restaurant.rating <= ratingFilter)
    if distanceFilter != '':
        if distanceFilter == 5:
            query = query.filter(Restaurant.distance <= 5)
        if distanceFilter == 10:
            query = query.filter(Restaurant.distance <= 10)
        if distanceFilter == 20:
            query = query.filter(Restaurant.distance <= 20)
        if distanceFilter == 21:
            query = query.filter(Restaurant.distance >= 20)

    sort_options = {
        'restaurant': Restaurant.name,
        'distance': Restaurant.distance,
        'rating': Restaurant.rating,
    }

    sort = request.args.get('sortBy', default='')
    order = request.args.get('order', default='')

    if sort in sort_options:
        if order == 'low':
            query = query.order_by(sort_options[sort])
        elif order == 'high':
            query = query.order_by(sort_options[sort].desc())

    rows = query.paginate(page=page, per_page=per_page, error_out=False)
    schema = restaurant_schema.dump(rows, many=True)
 
    num = query.count()
    return jsonify({"restaurants" : schema,
                    "meta": {
                        "count": num
                    }})

#INSTANCE RESTAURANT
@app.route('/restaurants/<int:restaurantId>',  methods=['GET'])
def get_restaurant_by_id(restaurantId):

    restaurant = db.session.query(Restaurant).filter_by(id=restaurantId).first()
    restaurant_data = restaurant_schema.dump(restaurant)
    if restaurant != None:
        return restaurant_data
    else: 
        status = ("Restaurant with id {id} not found.\n").format(id=restaurantId)
        return Response(status, status=404)


#GET ALLERGIES
@app.route('/allergyInfo', methods=['GET'])
def get_allergies():
    # Get the lifestyle value from the request
    lifestyle = request.args.get('lifestyle', default='')
    triggers = request.args.get('triggers', default='')
    ageGroup = request.args.get('ageGroup', default='')

    # Start the query
    query = db.session.query(Allergy)

    # Filter based on lifestyle value if provided and valid
    if lifestyle == 'yes':
        query = db.session.query(Allergy).filter(Allergy.lifestyle == True)
    elif lifestyle == 'no':
        query = db.session.query(Allergy).filter(Allergy.lifestyle == False)

    if ageGroup != '' and ageGroup != 'all':
        query = db.session.query(Allergy).filter(Allergy.commonAgeGroup.like("%" + ageGroup + "%"))
    elif ageGroup == 'all':
        query = db.session.query(Allergy).filter(Allergy.lifestyle == False)
    
    if triggers != '':
         query = db.session.query(Allergy).filter(Allergy.triggers.like("%" + triggers + "%"))

    sort_options = {
        'allergy': Allergy.title,
        'severity': Allergy.severity,
        'prevalence': Allergy.prevalence,
    }

    sort = request.args.get('sortBy', default='')
    order = request.args.get('order', default='')

    if sort in sort_options:
        if order == 'low':
            query = query.order_by(sort_options[sort])
        elif order == 'high':
            query = query.order_by(sort_options[sort].desc())

    # Pagination
    page = request.args.get('page', type=int, default=1)
    per_page = request.args.get('per_page', type=int, default=6)
    rows = query.paginate(page=page, per_page=per_page, error_out=False)
    schema = allergy_schema.dump(rows, many=True)

    # Total count
    num = query.count()

    return jsonify({"allergyInfo": schema,
                "meta": {
                    "count": num
                }})

#INSTANCE ALLERGY
@app.route('/allergyInfo/<int:allergyId>',  methods=['GET'])
def get_allergy_by_id(allergyId):

    allergy = db.session.query(Allergy).filter_by(id=allergyId).first()
    allergy_data = allergy_schema.dump(allergy)
    if allergy != None:
        return allergy_data
    else: 
        status = ("Allergy with id {id} not found.\n").format(id=allergyId)
        return Response(status, status=404)

#GET RELATED 
def is_it_true(value):
  return value.lower() == 'true'

@app.route('/similar',  methods=['GET'])
def get_similar_posts():    
    wants_recipe = request.args.get('recipe', type=is_it_true, default=False)
    wants_restaurant = request.args.get('restaurant', type=is_it_true, default=False)
    wants_allergy = request.args.get('allergy', type=is_it_true, default=False)

    result = {"similar" : {}}
    print(wants_recipe)
    if wants_recipe:

        while(True):
            id = random.randint(1, db.session.query(Recipe).count()) # Get's the ID of a random one
            recipe = db.session.query(Recipe).filter_by(id=id).first()
            recipe_data = recipe_schema.dump(recipe)
            if recipe != None:
                result["similar"].update({"recipe": recipe_data})
                break
    if wants_restaurant:
        while(True):
            id = random.randint(1, db.session.query(Restaurant).count()) # Get's the ID of a random one
            restaurant = db.session.query(Restaurant).filter_by(id=id).first()
            restaurant_data = restaurant_schema.dump(restaurant)
            if restaurant != None:
                result["similar"].update({"restaurant" : restaurant_data})
                break
    if wants_allergy:
        while(True):
            id = random.randint(1, db.session.query(Allergy).count()) # Get's the ID of a random one
            allergy = db.session.query(Allergy).filter_by(id=id).first()
            allergy_data = allergy_schema.dump(allergy)
            if allergy != None:
                result["similar"].update({"allergy" : allergy_data})
                break
    return result

if __name__ == '__main__':
    app.run(port=5000)
