#schema for json that is returned from RESTful API
from flask_marshmallow import Marshmallow

from models import Allergy, Restaurant, Recipe
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema, fields

ma = Marshmallow()

class AllergySchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Allergy

class RestaurantSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Restaurant
        
class RecipeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = Recipe

allergy_schema = AllergySchema()
restaurant_schema = RestaurantSchema()
recipe_schema = RecipeSchema()

