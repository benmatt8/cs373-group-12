import app
import unittest

app.app.config["TESTING"] = True
client = app.app.test_client()

class EndpointTests(unittest.TestCase):
    def test0(self):
        # Test Recipe Endpoint
        with client:
            response = client.get('/recipes')
            self.assertEqual(response.status_code, 200)
            data = response.json["recipes"]
            self.assertEqual(len(data), 6)
    
    def test1(self):
        # Test Retaurant Endpoint
        with client:
            response = client.get('/restaurants')
            self.assertEqual(response.status_code, 200)
            data = response.json["restaurants"]
            self.assertEqual(len(data), 6)

    def test2(self):
        # Test Allergy Endpoint
        with client:
            response = client.get('/allergyInfo')
            self.assertEqual(response.status_code, 200)
            data = response.json["allergyInfo"]
            self.assertEqual(len(data), 6)

    def test3(self):
        # Test Recipe id Endpoint
        with client:
            response = client.get('/recipes/QthmpFOrat4')
            self.assertEqual(response.status_code, 200)
            data = response.json
            self.assertEqual(data["title"], "4 Creamy Dairy-Free Pastas")
            self.assertEqual(data["channelTitle"], "Tasty")
    
    def test4(self):
        # Test Restaurant id Endpoint
        with client:
            response = client.get('/restaurants/1')
            self.assertEqual(response.status_code, 200)
            data = response.json
            self.assertEqual(data["id"], 1)
            self.assertEqual(data["cuisine"], "Bubble Tea")

    def test5(self):
        # Test Allergy id Endpoint
        with client:
            response = client.get('/allergyInfo/1')
            self.assertEqual(response.status_code, 200)
            data = response.json
            self.assertEqual(data["id"], 1)
            self.assertEqual(data["title"], "Milk Allergy")
            self.assertEqual(data["severity"], 3)


    def test6(self):
        # Test Similar Endpoint
        with client:
            response = client.get('/similar')
            self.assertEqual(response.status_code, 200)

    def test7(self):
        #Test Search Recipes
        with client:
            response = client.get('/search/recipes/vegetarian')
            self.assertEqual(response.status_code, 200)
            data = response.json
            self.assertEqual(data["meta"]["count"], 6)

    def test8(self):
        #Test Search Restaurants
        with client:
            response = client.get('/search/restaurants/vegan')
            self.assertEqual(response.status_code, 200)
            data = response.json
            for restaurant in data["restaurants"]:
                self.assertEqual(restaurant["cuisine"], "Vegan")

    def test9(self):
        #Test Search Allergy
        with client:
            response = client.get('/search/allergyinfo/dairy')
            self.assertEqual(response.status_code, 200)
            data = response.json
            for allergy in data["allergyInfo"]:
                self.assertEqual(allergy["description"].find("dairy") > -1, True)
if __name__ == "__main__":
    unittest.main()