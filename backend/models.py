from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_restful import Api, Resource, reqparse
from sqlalchemy import ForeignKey

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

app.config["SQLALCHEMY_DATABASE_URI"] = "mysql://user:SafeEats123$@safeeats-db.c1sq0s4ecml1.us-west-1.rds.amazonaws.com:3307/models"

db = SQLAlchemy(app)
ma = Marshmallow(app)

class Allergy(db.Model):
    __table_args__ = {'schema':'models'}
    __tablename__ = 'allergies'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.Text)
    severity = db.Column(db.Integer)
    prevalence = db.Column(db.Integer)
    allergy = db.Column(db.Boolean)
    lifestyle = db.Column(db.Boolean)
    description = db.Column(db.Text)
    imageUrl = db.Column(db.Text)
    link = db.Column(db.Text)
    commonAgeGroup = db.Column(db.Text)
    triggers = db.Column(db.Text)

class Restaurant(db.Model):
    __table_args__ = {'schema':'models'}
    __tablename__ = 'restaurants'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.Text)
    display_address = db.Column(db.Text)
    distance = db.Column(db.Float)
    cuisine = db.Column(db.Text)
    rating = db.Column(db.Float)
    allergens = db.Column(db.Text)
    price = db.Column(db.Text)
    image_url = db.Column(db.Text)
    display_phone = db.Column(db.String(25))
    is_closed = db.Column(db.Boolean)
    longitude = db.Column(db.Float)
    latitude = db.Column(db.Float)
    reviews = db.Column(db.Text)
    opening_hours = db.Column(db.Text)

class Recipe(db.Model):
    __table_args__ = {'schema':'models'}
    __tablename__ = 'recipes'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    videoId = db.Column(db.Text)
    title = db.Column(db.Text)
    channelTitle = db.Column(db.Text)
    description = db.Column(db.Text)
    publishTime = db.Column(db.String(25))
    likeCount = db.Column(db.Integer)
    viewCount = db.Column(db.Integer)
    commentCount = db.Column(db.Integer)
    imageUrl = db.Column(db.Text)
    allergyId = db.Column(ForeignKey("allergies.id"))

with app.app_context():
    db.create_all()

print ("Done")

if __name__ == '__main__':
    print ("MODELS MAIN CALLED")
    app.run(port=5000)