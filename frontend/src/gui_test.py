import unittest
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


URL = "https://www.safeats4you.lol/"
BURL = "https://platform.safeats4you.lol/"
#selenium_URL = os.getenv("http://selenium__standalone-chrome:4444/wd/hub", "http://localhost:4444/wd/hub")
class GUITest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        options = webdriver.ChromeOptions()
        options.add_experimental_option("excludeSwitches", ["enable-logging"])
        options.add_argument("--headless=new")
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
        chrome_prefs = {}
        options.experimental_options["prefs"] = chrome_prefs
        # Disable images
        chrome_prefs["profile.default_content_settings"] = {"images": 2}

        cls.driver = webdriver.Chrome(
            options=options, service=Service(ChromeDriverManager().install())
        )
        cls.driver.get(URL)
  
    def test0(self):
        # Test Nav Bar Presence
        self.driver.get(URL)
        self.driver.implicitly_wait(5)
        element = self.driver.find_element(By.CLASS_NAME, "navbar")
        self.assertEqual(element.is_displayed(), True)
    
    def test1(self):
        # Test Allergy Link
        self.driver.get(URL)
        element = self.driver.find_element(By.LINK_TEXT, "Allergy Info")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "allergyinfo")

    def test2(self):
        # Test Restaurants Link
        self.driver.get(URL)
        element = self.driver.find_element(By.LINK_TEXT, "Restaurants")
        element.click()
        self.assertEqual(self.driver.current_url, URL + "restaurants")

    def test3(self):
        # Test Recipes Link
        self.driver.get(URL)
        element = self.driver.find_element(By.LINK_TEXT, "Recipes")
        element.click()
        self.assertEqual(self.driver.current_url, URL + "recipes")

    def test4(self):
        # Test About Page
        self.driver.get(URL)
        element = self.driver.find_element(By.XPATH, "//*[@id='root']/nav/div/ul/li[8]/a")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "about")

    def test5(self):
        #Test Home Link
        self.driver.get(URL + "about")
        element = self.driver.find_element(By.XPATH, "//a[text() = 'Home']")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "home")

    def test6(self):
        # Test Recipe Nav Bar Link
        self.driver.get(URL)
        element = self.driver.find_element(By.XPATH, "//a[text() = 'Recipes']")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "recipes")

    def test7(self):
        # Test Restaurants Nav Bar Link
        self.driver.get(URL)
        element = self.driver.find_element(By.XPATH, "//a[text() = 'Restaurants']")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "restaurants")

    def test8(self):
        # Test Recipe Nav Bar Link
        self.driver.get(URL)
        element = self.driver.find_element(By.XPATH, "//a[text() = 'Allergy Info']")
        ref_link = element.get_attribute("href")
        self.assertEqual(ref_link, URL + "allergyinfo")

    def test9(self):
        #Test Card Link
        self.driver.get(URL + "restaurants")
        element = self.driver.find_element(By.XPATH, "//*[@id='root']/div[2]/div[5]/div[1]")
        element.click()
        self.assertEqual(self.driver.current_url, URL + "restaurants/Chicha%20San%20Chen")

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == "__main__":
    unittest.main()