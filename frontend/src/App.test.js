import { cleanup, render, screen, waitFor } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom';
import renderer from 'react-test-renderer'
import React from 'react';
import App from './App';
import About from './components/Pages/AboutUs/About';
import Recipes from './components/Pages/Models-Info,Restaurants,Recipes/Recipes/Recipes';
import Restaurants from './components/Pages/Models-Info,Restaurants,Recipes/Restaurants/Restaurants';
import AllergyInfo from './components/Pages/Models-Info,Restaurants,Recipes/AllergyInfo/AllergyInfo';
import { AllergyInfoDetails } from './components/Pages/Models-Info,Restaurants,Recipes/AllergyInfo/AllergyInfo-Details';
import { RestaurantDetails } from './components/Pages/Models-Info,Restaurants,Recipes/Restaurants/Restaurant-Details';
import { RecipeDetails } from './components/Pages/Models-Info,Restaurants,Recipes/Recipes/Recipe-Details';

afterEach(() => {
  cleanup();
})


test('Test presence of NavBar', async () => {
  render(<App />);
  await waitFor(() => {
    const linkElement = screen.getByText('About');
    expect(linkElement).toBeInTheDocument();
  });
});

test('Test About Page', async () => {
  render(<About />);
  const element = screen.getByText('About SafeEats for You');
  expect(element).toBeInTheDocument();
});

test('Test About Page Loading', async () => {
  render(<About />);
  const element = screen.getByText('Loading Contributors..');
  expect(element).toBeInTheDocument();
});

test('Test Recipe Page', async () => {
  const tree = <BrowserRouter> renderer.create(<Recipes />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('Test Restaurant Page', async () => {
  const tree = <BrowserRouter> renderer.create(<Restaurants />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('Test Allergy Page', async () => {
  const tree = <BrowserRouter> renderer.create(<AllergyInfo />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('About Page Render', async () => {
  const tree = <BrowserRouter> renderer.create(<About />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('Test Allergy Card', async () => {
  const tree = <BrowserRouter> renderer.create(<AllergyInfoDetails />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('Test Restaurant Card', async () => {
  const tree = <BrowserRouter> renderer.create(<RestaurantDetails />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})

test('Test Recipe Card', async () => {
  const tree = <BrowserRouter> renderer.create(<RecipeDetails />).toJSON </BrowserRouter>
  expect(tree).toMatchSnapshot()
})