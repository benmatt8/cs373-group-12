import * as React from "react";
import Box from "@mui/material/Box";

export const CritquesPage = () => {
  return (
    <div style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
      <div style={{ maxWidth: "1000px" }}>
        <Box
          sx={{
            backgroundColor: "#f9f9f9",
            padding: "20px",
            borderRadius: "10px",
            maxWidth: "1000px",
            textAlign: "center",
          }}
        >
          <div style={{ fontSize: "small", color: "black" }}>
            <h1><em>Self Critiques for Our Website</em></h1>
            <div style={{ marginBottom: "20px" }}>
              <br></br>
              <h2>What did we do well?</h2>
              <p> 
                Our website was designed to be easily accessible and navigable,
                providing a user-friendly experience for your audience. We recognized
                the importance of providing comprehensive information, such as
                recipes with multiple media types and tagging restaurants with more
                allergies to accommodate a broader audience.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What did we learn?</h2>
              <p>
                We gained skills in backend, learning how to implement features such
                as sorting, filtering, and searching. We also had limited knowledge
                of how to use APIs before starting, and this project taught us how to
                create endpoints, put data from APIs into our database, and pull that
                data to use it on our frontend. We also just learned more about the
                topic and growing issues of allergies overall, as well as how to
                maybe provide a safe space for people with such restrictions and add
                inclusivity in dining experiences.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What did we teach each other?</h2>
              <p>
                Each team member brought different skills and knowledge to the
                project, with varying levels of experience with React, SQL, and
                Python. We were all able to share these skills and teach each other
                how to implement certain features, set up a database, and create
                better UI designs. Collaboratively, we incorporated new elements into
                the website based on the insights and contributions of each team
                member.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What can we do better?</h2>
              <p>
                Continuing to refine and enhance the website's features based on user
                feedback and evolving needs can lead to further improvements. We can
                also add more allergies, and tag recipes and restaurants with
                multiple allergies as well, so that we are able to reach a broader
                audience and meet the needs of more people. We can also find recipes
                on sites other than Youtube to allow for more diverse and accessible
                content.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What effect did the peer reviews have?</h2>
              <p>
                Peer reviews provided valuable insights and constructive feedback,
                helping identify strengths and areas for improvement in the website.
                Due to it, we were able to make our UI look more appealing, and we
                provided details on our instances that were more useful for UT Austin
                students, making the website relevant for our audience.
              </p>
            </div>
            <div>
              <h2>What puzzles us?</h2>
              <p>
                We do have some areas where we have redundant code so why not try to
                make it reusable instead since there would be less of a hassle
                debugging.
              </p>
            </div>
          </div>
        </Box>
      </div>
      <div style={{ maxWidth: "1000px", marginTop: "100px" }}>
        <Box
          sx={{
            backgroundColor: "#f9f9f9",
            padding: "20px",
            borderRadius: "10px",
            marginBottom: "80px",
            maxWidth: "1000px",
            textAlign: "center",
          }}
        >
          <div style={{ fontSize: "small", color: "black" }}>
            <h1>
              <em>
                Critiques for <a href="https://www.substance-abuse.me/" target="_blank" rel="noopener noreferrer">Substance Abuse Awareness</a>
              </em>
            </h1>
            <br></br>
            <div style={{ marginBottom: "20px" }}>
              <h2>What did they do well?</h2>
              <p>
                They effectively provided essential information to their audience,
                including details about treatment centers and city statistics,
                addressing user needs and user stories effectively.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>How effective was their RESTful API?</h2>
              <p>
                The RESTful API worked well, enabling them to retrieve the necessary
                information and display it to their audience seamlessly. This
                indicates that their API implementation was effective and reliable.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>How well did they implement your user stories?</h2>
              <p>
                Their implementation of user stories appears to have been successful,
                as they managed to address the needs and requirements outlined in the
                user stories effectively. This suggests that they understood and
                incorporated user feedback into their website design.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What did we learn from their website?</h2>
              <p>
                From their website, we learned the importance of providing
                comprehensive information to users, as well as the effectiveness of
                using RESTful APIs to fetch and display relevant data. Additionally,
                their focus on addressing user stories highlights the significance of
                aligning website features with user needs. We also learned more about
                the issue of substance abuse in this country, important resources for
                those that are struggling, and how to help overall.
              </p>
            </div>
            <div style={{ marginBottom: "20px" }}>
              <h2>What can they do better?</h2>
              <p>
                They can improve their website by incorporating more visuals that can
                captivate and engage the audience further. Adding visually appealing
                elements can enhance user experience and encourage users to explore
                more of the website's content.
              </p>
            </div>
            <div>
              <h2>What puzzles us about their website?</h2>
              <p>
                Currently, their city statistics page looks out of place compared to
                their other models so that’s weird. Other than that, there’s not
                really much that puzzles us.
              </p>
            </div>
          </div>
        </Box>
      </div>
    </div>
  );
};

export default CritquesPage;
