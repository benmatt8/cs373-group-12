import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';


const restaurantAPIData = [];

export const Restaurants = () => {
  const [data, setData] = useState([]);
  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(6); // Amount of cards to display per page
  const [numInstances, setMetaData] = useState(1);
  const [sortMenuAnchor, setSortMenuAnchor] = useState(null);

  const fetchData = async (pageNum) => {
    try {
      const response = await fetch(`${window.backendHost}/restaurants?page=${pageNum}&per_page=${perPage}`);
      if (response.ok) {
        const data = await response.json();
        setData(data.restaurants);
        setMetaData(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchData(pageNum);
  }, [pageNum]);


  const handleFilterLifestyle = (filterValue) => {
  };

  const handleSortOrder = (order) => {
  };

  const handleSortByCriteria = (criteria) => {
  };

  return (
    <>
      <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
        <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
          Results: {pageNum * perPage} of {numInstances}
        </Typography>
      </div>
      <Box sx={{ display: "flex", justifyContent: "center", color: "white" }}>
        {/* Sort By Button */}
        <Button
          id="basic-button"
          variant="outlined"
          onClick={(event) => setSortMenuAnchor(event.currentTarget)} // Open the sort menu
        >
          Sort By: 
        </Button>
        {/* Sort By Menu */}
        <Menu
          id="basic-menu"
          anchorEl={sortMenuAnchor}
          open={Boolean(sortMenuAnchor)}
          onClose={() => setSortMenuAnchor(null)} // Close the sort menu
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={() => handleSortByCriteria("distance")}>Restaurant Name</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria("distance")}>Distance</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria("cost")}>Rating</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria("rating")}>Cost</MenuItem>
        </Menu>
        <ToggleButtonGroup
          exclusive
          onChange={(event, newSortOrder) => handleSortOrder(newSortOrder)}
          aria-label="sort order"
        >
          <ToggleButton value="low" aria-label="asc" sx={{ color: "white" }}>
            Ascending
          </ToggleButton>
          <ToggleButton value="high" aria-label="desc" sx={{ color: "white" }}>
            Descending
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
      <Select  label="Age" style={{ position: 'relative', zIndex: 0 }}>
        <MenuItem disabled>Select an Option</MenuItem>
        <MenuItem value="Allergry 1">Allergy 1</MenuItem>
        <MenuItem value="Allergry 2">Allergy 2</MenuItem>
        <MenuItem value="Allergry 3">Allergy 3</MenuItem>
      </Select>

      <Select >
    <MenuItem disabled>Select an Option</MenuItem>
      <MenuItem value="Allergry 1">Allergry 1</MenuItem>
      <MenuItem value="Allergry 2">Allergry 2</MenuItem>
      <MenuItem value="Allergry 3">Allergry 3</MenuItem>
      </Select>
      
      <Box textAlign='center' sx={{ marginTop: '10px' }}>
        <Button variant="contained" sx={{ marginLeft: "10px" }}>
          Reset
        </Button>
      </Box>
      <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
        {data.map((restaurant, index) => (
          <div style={{ width: "30%", marginBottom: "20px" }}>
            <Link key={index} to={`/restaurants/${restaurant.name}`} state={restaurant} style={{ textDecoration: 'none' }}>
              <Card
                style={{
                  width: "100%",
                  height: "90%",
                  marginBottom: "80px",
                  boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
                  backgroundColor: "#1a1d1e",
                  color: "#fff"
                }}
              >
                <CardMedia
                  component="img"
                  sx={{ height: 250, width: "100%" }}
                  image={restaurant.image_url}
                  alt="image"
                  title={restaurant.name}
                />
                <CardContent>
                  {/* Displays filterable/sortable information on restaurant card */}
                  <Typography gutterBottom variant="h5" component="div">
                    {restaurant.name}
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Address:</strong> {restaurant.display_address}
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Distance from UT Austin:</strong> {restaurant.distance} miles
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Cuisine:</strong> {restaurant.cuisine}
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Rating:</strong> {restaurant.rating}
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Allergens:</strong> {restaurant.allergens}
                  </Typography>
                  <Typography variant="body2" color="#94999b">
                    <strong>Price:</strong> {restaurant.price !== null ? restaurant.price : "N/A"}
                  </Typography>

                  <Typography variant="body2" color="#94999b">
                    <Link to={`/recipes/${restaurant.allergens}`}>
                      Click to view Related Recipes
                    </Link>
                  </Typography>
                </CardContent>
              </Card>
            </Link>
          </div>
        ))}
      </div>
      <div style={{ textAlign: 'center', marginBottom: '20px' }}>
 <button
   onClick={() => setPageNum((prev) => prev - 1)}
   disabled={pageNum === 1}
   style={{
     padding: '8px 16px',
     backgroundColor: pageNum > 1 ? 'green' : 'gray',
     color: 'white',
     border: 'none',
     borderRadius: '4px',
     cursor: 'pointer',
     outline: 'none'
   }}
 >
   Previous
 </button>

 <span style={{ color: "#afdff0", fontSize: '20px', marginRight: '10px', marginLeft: '10px' }}>
   Page {pageNum} of {Math.ceil(numInstances / perPage)}
 </span>

 <button
   onClick={() => {
     if (data.length > 0) {
       setPageNum((prev) => prev + 1);
     }
   }}
   disabled={pageNum === Math.ceil(numInstances / perPage)}
   style={{
     padding: '8px 16px',
     backgroundColor: pageNum < Math.ceil(numInstances / perPage) ? 'green' : 'gray',
     marginButton: '2px',
     color: 'white',
     border: 'none',
     borderRadius: '4px',
     cursor: 'pointer',
     outline: 'none'
   }}
 >
   Next
 </button>
</div>

</>
);
};
export { restaurantAPIData };