import * as React from 'react';
import { useParams, useLocation } from "react-router-dom";
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Link } from 'react-router-dom';
import { useEffect, useState } from "react";
// import {allergyAPIData} from './AllergyInfo'

export const AllergyInfoDetails = () => {
  const [similarData, setSimilarData] = useState(null);
  const [getSimilarData, setSimilarBool] = useState(true);
  const { state } = useLocation();
  window.scrollTo(0, 0);

  const fetchSimilarData = async (getSimilarData) => {
    try {
      const getRecipe = true;
      const getRestaurant = true;
      const getAllergy = false;
      const response = await fetch(`${window.backendHost}/similar?recipe=${getRecipe}&restaurant=${getRestaurant}&allergy=${getAllergy}`);
      if (response.ok) {
        const data = await response.json();
        setSimilarData(data.similar);

      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchSimilarData(getSimilarData);
  }, [getSimilarData]);

  return similarData && (
      <>
      {/*Card layout per instance of each allergy*/}
      <Card style={{margin: 'auto', maxWidth: '800px', backgroundColor: "#434765", color: "white"}}>
          <CardMedia
              component="img"
              height="300"
              image={require(`${state.imageUrl}`)}
              alt={state.title}
          />
          <CardContent>
              <Typography variant="h5" component="h2" style={{ marginBottom: '20px' }}>
                  <Link to={`${state.link}`} target="_blank" style={{ textDecoration: 'none', color: 'inherit' }}>
                      {state.title}
                  </Link>
              </Typography>
              <Typography variant="body2" color="white">
                <strong>Kind:</strong> {state.title} <br />
                <strong>Severity:</strong> {state.severity} <br />
                <strong>Prevalence:</strong> {state.prevalence} <br />
                <strong>Allergy:</strong> {state.allergy ? 'Yes' : 'No'} <br />
                <strong>Lifestyle:</strong> {state.lifestyle ? 'Yes' : 'No'} <br />
              </Typography>
              <Typography variant="body1" style={{ marginTop: '20px' }}>
                  {state.description}
              </Typography>
              {/*imbeds web.md website per allergy*/}
              <iframe src={`${state.link}`} className="custom-iframe" title="WebMD" frameBorder="0" width="100%" height="600px"></iframe>
          </CardContent>
      </Card>

      <Typography variant="h5" style={{ marginTop: '50px', textAlign: 'center', color: "#e5dfda" }}>
          Similar pages you might be interested in:
      </Typography>
      {/*cards linked to other instances related to the current page*/}
      <div style={{ display: 'flex', justifyContent: 'space-between', margin: '20px auto', maxWidth: '1200px' }}>
        <Link to={`/restaurants/${similarData.restaurant.name}`} state={similarData.restaurant} style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#fff"
          }}>
            <CardMedia
              component="img"
              height="400"
              image={similarData.restaurant.image_url}
              alt={similarData.restaurant.name}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div" color="#e5dfda">
                {similarData.restaurant.name}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Address:</strong> {similarData.restaurant.display_address}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Distance from UT Austin:</strong> {similarData.restaurant.distance}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Cuisine:</strong> {similarData.restaurant.cuisine}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Rating:</strong> {similarData.restaurant.rating}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Allergens:</strong> {similarData.restaurant.allergens}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Price:</strong> {similarData.restaurant.price}
              </Typography>
            </CardContent>
          </Card>
        </Link>

        <Link to={`/recipes/${similarData.recipe.videoId}`} state={similarData.recipe}  style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#fff"
          }}>
            <CardMedia
              component="img"
              height="500"
              image={similarData.recipe.imageUrl}
              alt="image"
            />
            <CardContent>
              <Typography variant="body2" sx={{ fontSize: "23px", color: "#e5dfda" }}>
                {similarData.recipe.title}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Channel:</strong> {similarData.recipe.channelTitle}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Description:</strong> {similarData.recipe.description.substring(0, 30)}...
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Published:</strong> {similarData.recipe.publishTime.substring(0, 10)}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Views:</strong> {similarData.recipe.viewCount}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Likes:</strong> {similarData.recipe.likeCount}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Comments:</strong> {similarData.recipe.commentCount}
              </Typography>
            </CardContent>
          </Card>
        </Link>
      </div> 
    </>
  );
}
