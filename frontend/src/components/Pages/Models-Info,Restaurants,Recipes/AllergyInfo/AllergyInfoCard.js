import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

const AllergyInfoCard = ({ allergy, searchTerm }) => {

  const getHighlightedText = (text, highlight) =>{
    // Split text on highlight term, include term itself into parts, ignore case
    const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
    console.log(parts);
    return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <b style={{backgroundColor: "#87876B"}}>{part}</b> : part)}</span>;
};
  return (

    <Link to={`/allergyInfo/${allergy.id}`} state={allergy} style={{ textDecoration: 'none', width: "30%", marginBottom: "80px", marginLeft: "20px", marginRight: "20px" }}>
      <Card
        style={{
          width: "100%",
          height: "90%",
          marginBottom: "80px",
          boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
          backgroundColor: "#636684",
          color: "#fff"
        }}
      >
        <CardMedia
          sx={{ height: 250, width: "100%" }}
          image={require(`${allergy.imageUrl}`)}
          title={allergy.title}
        />
        <CardContent>
          <Typography variant="body2" sx={{ fontSize: "23px", color: "#e5dfda",  fontWeight: "bold", fontFamily: "Arial, sans-serif" }}>
            {getHighlightedText(allergy.title, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda" marginTop= "10px">
            <strong>Kind:</strong> {getHighlightedText(allergy.title, searchTerm)} <br />
            <strong>Severity:</strong> {allergy.severity} <br />
            <strong>Prevalence:</strong> {allergy.prevalence} <br />
            <strong>Allergy:</strong> {allergy.allergy ? 'Yes' : 'No'} <br />
            <strong>Lifestyle:</strong> {allergy.lifestyle ? 'Yes' : 'No'} <br />
            <strong>Triggers:</strong> {allergy.triggers} <br />
            <strong>Affected Age Group:</strong> {allergy.commonAgeGroup} <br />
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

export default AllergyInfoCard;
