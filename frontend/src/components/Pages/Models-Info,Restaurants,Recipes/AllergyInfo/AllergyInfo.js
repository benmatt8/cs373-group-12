import React, { useState, useEffect, useRef } from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Link } from 'react-router-dom';
import { FaSearch } from "react-icons/fa";
import { FaCaretDown } from "react-icons/fa";
import AllergyCard from './AllergyInfoCard';


const allergyAPIData = [];

export const AllergyInfo = () => {
  const [lifestyle, setLifestyle] = React.useState('');
  const [ageGroup, setAgeGroup] = React.useState('');
  const [triggers, setTriggers] = React.useState('');
  const [sortBy, setSortBy] = useState(''); // State for sorting
  const [order, setOrder] = useState('');
  
  const [searchQuery, setSearchQuery] = useState(''); // State for search query

  const [data, setData] = useState([]);
  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(6); // Amount of cards to display per page
  const [numInstances, setMetaData] = useState(1);
  const [sortMenuAnchor, setSortMenuAnchor] = useState(null); // State for anchor element of sort menu

  const fetchData = async (pageNum, lifestyle, ageGroup, triggers, order, sortBy, searchQuery) => {
    try {
      let response = '';
      if (searchQuery === '') {
        response = await fetch(`${window.backendHost}/allergyInfo?page=${pageNum}&per_page=${perPage}&ageGroup=${ageGroup}&triggers=${triggers}&lifestyle=${lifestyle}&order=${order}&sortBy=${sortBy}`);
      } else {
        response = await fetch(`${window.backendHost}/search/allergyInfo/${searchQuery}?page=${pageNum}&per_page=${perPage}`);
      }
      
      if (response.ok) {
        const data = await response.json();
        console.log("Data received from server:", data); 
        setData(data.allergyInfo);
        setMetaData(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSearch();
    }
  };
  
  useEffect(() => {
    fetchData(pageNum, lifestyle, ageGroup, triggers, order, sortBy, searchQuery);
  }, [pageNum, lifestyle, ageGroup, triggers, order, sortBy, searchQuery]);


  const handleLifestyleFilter = (event) => {
    setLifestyle(event.target.value);
  };

  const handleAgeGroupFilter = (event) => {
    setAgeGroup(event.target.value);
  };

  const handleTriggersFilter = (event) => {
    setTriggers(event.target.value);
  };


  const handleOrder = (event) => {
    setOrder(event.target.value);
  };

  const handleSortByCriteria = (criteria) => {
    setSortBy(criteria);
  };
  
  const inputRef = useRef(null);
  const handleSearch = () => {
    console.log(inputRef.current.value);
    setSearchQuery(inputRef.current.value);
  };

  const handleReset = () => {
    setOrder('');
    setSortBy('');
    setLifestyle('');
    setAgeGroup('');
    setTriggers('');
    setSearchQuery('');
  };
  
  return (
    <>
     <h1 style={{ fontFamily: "cursive", fontSize: "3rem", textShadow: "2px 2px 4px #000", color: "#7CB342" }}>Allergies</h1>
      <>
      {/* change for search bar */}
      <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center' }}>
  <input
    type="text"
    placeholder="Search..."
    ref={inputRef}
    style={{
      marginBottom: '10px',
      padding: '6px', /* Decreased padding for a smaller size */
      width: '25%', /* Reduced width for a smaller size */
      height: '25px', /* Reduced height for a smaller size */
      borderRadius: '12px', /* Rounded corners for a modern look */
      border: '2px solid #3498db', /* Changed border color */
      boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)', /* Added box shadow for depth */
      backgroundColor: '#ADD8E6' /* Pastel light green color */
    }}
    onKeyPress={handleKeyPress}
  />
  <button
    onClick={handleSearch}
    style={{
      backgroundColor: '#3498db', /* Changed background color */
      border: 'none', /* Removed border for cleaner appearance */
      height: '35px', /* Matched button height with input */
      width: '30px',
      marginLeft: '3px', /* Adjusted margin for alignment */
      padding: '6px', /* Decreased padding for a smaller size */
      borderRadius: '12px', /* Rounded corners for button */
      cursor: 'pointer', /* Cursor pointer for indicating interactivity */
      marginTop: '2.5px' /* Adjusted marginTop to move the button up */
    }}
  >
    <FaSearch color="white" size={12} /> {/* Reduced icon size for a smaller size */}
  </button>
</div>

      <Box sx={{ display: "flex", justifyContent: "center", color: "white" }}>
        <Button
          id="basic-button"
          variant="outlined"
          onClick={(event) => setSortMenuAnchor(event.currentTarget)} 
        >
          Sort By {sortBy} <FaCaretDown />
        </Button>
        {/* Sort By Menu */}
        <Menu
          id="basic-menu"
          anchorEl={sortMenuAnchor}
          open={Boolean(sortMenuAnchor)}
          onClose={() => setSortMenuAnchor(null)} 
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={() => handleSortByCriteria('')}><em>None</em></MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('allergy')}>Allergy</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('severity')}>Severity</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('prevalence')}>Prevalence</MenuItem>
        </Menu>
        <ToggleButtonGroup
          exclusive
          onChange={handleOrder}
          aria-label="sort order"
        >
          <ToggleButton
            value="low"
            aria-label="asc"
            sx={{
              color: 'white',
              backgroundColor: order === 'low' ? '#AEC6CF' : 'transparent',
              '&:hover': {
                backgroundColor: order === 'low' ? '#AEC6CF' : 'gray', 
              },
            }}
          >
            Ascending
          </ToggleButton>
            <ToggleButton
              value="high"
              aria-label="desc"
              sx={{
                color: 'white',
                backgroundColor: order === 'high' ? '#AEC6CF' : 'transparent', 
                '&:hover': {
                  backgroundColor: order === 'high' ? '#AEC6CF' : 'gray', 
                },
              }}
            >
            Descending
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
      <div>
      <FormControl sx={{ m: 1, minWidth: 170 }}>
        <InputLabel id="demo-simple-select-helper-label"sx={{ color: 'white', text: '40px' }}>Lifestyle Choice</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={lifestyle}
          label="Lifestyle Choice"
          onChange={handleLifestyleFilter}
        >
          <MenuItem value=''>
            <em>None</em>
          </MenuItem>
          <MenuItem value={'yes'}>Lifestyle Choice</MenuItem>
          <MenuItem value={'no'}>Not a Lifestyle Choice</MenuItem>
        </Select>
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 170 }}>
        <InputLabel id="demo-simple-select-helper-label" sx={{ color: 'white', text: '40px' }}>Age Group</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={ageGroup}
          label="Age Group"
          onChange={handleAgeGroupFilter}
        >
          <MenuItem value=''>
            <em>None</em>
          </MenuItem>
          <MenuItem value={'Children'}>Children</MenuItem>
          <MenuItem value={'Adults'}>Adults</MenuItem>
          <MenuItem value={'Infants'}>Infants</MenuItem>
          <MenuItem value={'all'}>All Ages</MenuItem>
        </Select>
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 170 }}>
        <InputLabel id="demo-simple-select-helper-label"sx={{ color: 'white', text: '40px' }}>Triggers</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={triggers}
          label="Triggers"
          onChange={handleTriggersFilter}
        >
          <MenuItem value=''>
            <em>None</em>
          </MenuItem>
          <MenuItem value={'Genetics'}>Genetics</MenuItem>
          <MenuItem value={'Proteins'}>Proteins</MenuItem>
          <MenuItem value={'Immune system sensitivity'}>Immune system sensitivity</MenuItem>
          <MenuItem value={'Environmental factors'}>Environmental factors</MenuItem>
          <MenuItem value={'Leaky gut syndrome'}>Leaky gut syndrome</MenuItem>
          <MenuItem value={'Cross-reactivity'}>Cross-reactivity</MenuItem>
          <MenuItem value={'Preservatives'}>Preservatives</MenuItem>
          <MenuItem value={'Compounds'}>Compounds</MenuItem>
          <MenuItem value={'Ingestion'}>Ingestion</MenuItem>
        </Select>
      </FormControl>
    </div>
    <Box textAlign='center' sx={{ marginTop: '-15px', marginBottom: '25px' }}>
      <Button variant="contained" sx={{ marginLeft: "10px", bgcolor: "#3498db", color: "#ffffff" }} onClick={handleReset}>
        Reset
      </Button>
    </Box>
      </>
      <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
        {data.map((Allergy, index) => (
            <AllergyCard key={index} allergy={Allergy} searchTerm = {searchQuery}/>
        ))}
      </div>
      <div style={{ textAlign: 'center', marginBottom: '20px' }}>
        <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
          <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
            Results: {Math.min(pageNum * perPage, numInstances)} of {numInstances}
          </Typography>
        </div>
        <button
          onClick={() => setPageNum((prev) => prev - 1)}
          disabled={pageNum === 1}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum > 1 ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginRight: '10px'
          }}
        >
          Previous
        </button>

        {[...Array(Math.ceil(numInstances / perPage)).keys()].map((pageNumber) => (
          <button
            key={pageNumber + 1}
            onClick={() => setPageNum(pageNumber + 1)}
            style={{
              padding: '8px 16px',
              backgroundColor: pageNum === pageNumber + 1 ? 'green' : 'gray',
              color: 'white',
              border: 'none',
              borderRadius: '4px',
              cursor: 'pointer',
              outline: 'none',
              marginRight: '5px'
            }}
          >
            {pageNumber + 1}
          </button>
        ))}

        <button
          onClick={() => {
            if (data.length > 0) {
              setPageNum((prev) => prev + 1);
            }
          }}
          disabled={pageNum === Math.ceil(numInstances / perPage)}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum < Math.ceil(numInstances / perPage) ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginLeft: '10px'
          }}
        >
          Next
        </button>
      </div>

    </>
  );
};

export { allergyAPIData }
