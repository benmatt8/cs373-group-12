import React, { useState, useEffect, useRef } from "react";
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { FaSearch } from "react-icons/fa";
import { FaCaretDown } from "react-icons/fa";
import RestaurantCard from "./RestaurantCard";

const restaurantAPIData = [];

export const Restaurants = () => {
  const [allergy, setAllergy] = React.useState('');
  const [cuisine, setCuisine] = React.useState('');
  const [distance, setDistance] = React.useState('');
  const [rating, setRating] = React.useState('');
  const [order, setOrder] = React.useState('');
  const [sortBy, setSortBy] = React.useState('');

  // FOR SEARCH
  const [searchQuery, setSearchQuery] = useState(''); // State for search query

  const [data, setData] = useState([]);
  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(6); // Amount of cards to display per page
  const [numInstances, setMetaData] = useState(1);
  const [sortMenuAnchor, setSortMenuAnchor] = useState(null);

  const fetchData = async (pageNum, allergy, cuisine, distance, rating, order, sortBy, searchQuery) => {
    try {
      let response = '';
      if (searchQuery === '') {
        response = await fetch(`${window.backendHost}/restaurants?page=${pageNum}&per_page=${perPage}&cuisine=${cuisine}&allergy=${allergy}&distance=${distance}&rating=${rating}&order=${order}&sortBy=${sortBy}`);
      } else {
        response = await fetch(`${window.backendHost}/search/restaurants/${searchQuery}?page=${pageNum}&per_page=${perPage}`);
      }

      if (response.ok) {
        const data = await response.json();
        setData(data.restaurants);
        setMetaData(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSearch();
    }
  };


  useEffect(() => {
    fetchData(pageNum, allergy, cuisine, distance, rating, order, sortBy, searchQuery);
  }, [pageNum, allergy, cuisine, distance, rating, order, sortBy, searchQuery]);


  const handleAllergyFilter = (event) => {
    setAllergy(event.target.value);
  };

  const handleCuisineFilter = (event) => {
    setCuisine(event.target.value);
  };

  const handleDistanceFilter = (event) => {
    setDistance(event.target.value);
  };

  const handleRatingFilter = (event) => {
    setRating(event.target.value);
  };

  const handleOrder = (event) => {
    setOrder(event.target.value);
  };

  const handleSortByCriteria = (criteria) => {
    setSortBy(criteria);
  };

  const inputRef = useRef(null);
  const handleSearch = () => {
    console.log(inputRef.current.value);
    setSearchQuery(inputRef.current.value);
  };

  const handleReset = () => {
    setOrder('');
    setSortBy('');
    setAllergy('');
    setCuisine('');
    setDistance('');
    setRating('');
    setSearchQuery('');
  };

  return (
    <>
     <h1 style={{ fontFamily: "cursive", fontSize: "3rem", textShadow: "2px 2px 4px #000", color: "#7CB342" }}>Restaurants</h1>
      {/* change for search bar */}
      <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center' }}>
  <input
    type="text"
    placeholder="Search..."
    ref={inputRef}
    style={{
      marginBottom: '10px',
      padding: '6px', /* Decreased padding for a smaller size */
      width: '25%', /* Reduced width for a smaller size */
      height: '25px', /* Reduced height for a smaller size */
      borderRadius: '12px', /* Rounded corners for a modern look */
      border: '2px solid #3498db', /* Changed border color */
      boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)', /* Added box shadow for depth */
      backgroundColor: '#ADD8E6' /* Pastel light green color */
    }}
    onKeyPress={handleKeyPress}
  />
  <button
    onClick={handleSearch}
    style={{
      backgroundColor: '#3498db', /* Changed background color */
      border: 'none', /* Removed border for cleaner appearance */
      height: '35px', /* Matched button height with input */
      width: '30px',
      marginLeft: '3px', /* Adjusted margin for alignment */
      padding: '6px', /* Decreased padding for a smaller size */
      borderRadius: '12px', /* Rounded corners for button */
      cursor: 'pointer', /* Cursor pointer for indicating interactivity */
      marginTop: '2.5px' /* Adjusted marginTop to move the button up */
    }}
  >
    <FaSearch color="white" size={12} /> {/* Reduced icon size for a smaller size */}
  </button>
</div>

      <Box sx={{ display: "flex", justifyContent: "center", color: "white" }}>
        {/* Sort By Button */}
        <Button
          id="basic-button"
          variant="outlined"
          onClick={(event) => setSortMenuAnchor(event.currentTarget)} 
        >
          Sort By {sortBy} <FaCaretDown />
        </Button>
        {/* Sort By Menu */}
        <Menu
          id="basic-menu"
          anchorEl={sortMenuAnchor}
          open={Boolean(sortMenuAnchor)}
          onClose={() => setSortMenuAnchor(null)} 
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={() => handleSortByCriteria('')}><em>None</em></MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('restaurant')}>Restaurant Name</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('distance')}>Distance</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('rating')}>Rating</MenuItem>
        </Menu>
        <ToggleButtonGroup
          exclusive
          onChange={handleOrder}
          aria-label="sort order"
        >
          <ToggleButton
            value="low"
            aria-label="asc"
            sx={{
              color: 'white',
              backgroundColor: order === 'low' ? '#AEC6CF' : 'transparent', 
              '&:hover': {
                backgroundColor: order === 'low' ? '#AEC6CF' : 'gray',
              },
            }}
          >
            Ascending
          </ToggleButton>
          <ToggleButton
            value="high"
            aria-label="desc"
            sx={{
              color: 'white',
              backgroundColor: order === 'high' ? '#AEC6CF' : 'transparent', // Change background color when selected
              '&:hover': {
                backgroundColor: order === 'high' ? '#AEC6CF' : 'gray', // Change background color on hover
              },
            }}
          >
            Descending
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
      <div>
      <FormControl sx={{ m: 1, minWidth: 150 }}>
        <InputLabel id="demo-simple-select-helper-label"sx={{ color: 'white', text: '40px' }}>Allergy</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={allergy}
          label="Allergy Choice"
          onChange={handleAllergyFilter}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
            <MenuItem value={"Milk Allergy"}> Milk Allergy</MenuItem>
            <MenuItem value={"Shellfish Allergy"}> Shellfish Allergy</MenuItem>
            <MenuItem value={"Soy Allergy"}> Soy Allergy</MenuItem>
            <MenuItem value={"Eggs Allergy"}> Eggs Allergy</MenuItem>
            <MenuItem value={"Fish Allergy"}> Fish Allergy</MenuItem>
            <MenuItem value={"Crustacean Shellfish Allergy"}> Crustacean Shellfish Allergy</MenuItem>
            <MenuItem value={"Tree Nuts Allergy"}> Tree Nuts Allergy</MenuItem>
            <MenuItem value={"Peanuts Allergy"}> Peanuts Allergy</MenuItem>
            <MenuItem value={"Wheat Allergy"}> Wheat Allergy</MenuItem>
            <MenuItem value={"Sesame Allergy"}> Sesame Allergy</MenuItem>
            <MenuItem value={"Sulfite Allergy"}> Sulfite Allergy</MenuItem>
            <MenuItem value={"Mustard Allergy"}> Mustard Allergy</MenuItem>
            <MenuItem value={"Buckwheat Allergy"}> Buckwheat Allergy</MenuItem>
            <MenuItem value={"Celery Allergy"}> Celery Allergy</MenuItem>
            <MenuItem value={"Lupin Allergy"}> Lupin Allergy</MenuItem>
            <MenuItem value={"Mango Allergy"}> Mango Allergy</MenuItem>
            <MenuItem value={"Peach Allergy"}> Peach Allergy</MenuItem>
            <MenuItem value={"Pork Allergy"}> Pork Allergy</MenuItem>
            <MenuItem value={"Tomato Allergy"}> Tomato Allergy</MenuItem>
            <MenuItem value={"Strawberry Allergy"}> Strawberry Allergy</MenuItem>
            <MenuItem value={"Garlic Allergy"}> Garlic Allergy</MenuItem>
            <MenuItem value={"Oats Allergy"}> Oats Allergy</MenuItem>
            <MenuItem value={"Balsam of Peru Allergy"}> Balsam of Peru Allergy</MenuItem>
            <MenuItem value={"Maize Allergy"}> Maize Allergy</MenuItem>
            <MenuItem value={"Cinnamon Allergy"}> Cinnamon Allergy</MenuItem>
            <MenuItem value={"Meat Allergy"}> Meat Allergy</MenuItem>
            <MenuItem value={"Apple Allergy"}> Apple Allergy</MenuItem>
            <MenuItem value={"Gluten-Free Diet"}> Gluten-Free Diet</MenuItem>
            <MenuItem value={"Vegan Diet"}> Vegan Diet</MenuItem>
            <MenuItem value={"Vegetarian Diet"}> Vegetarian Diet</MenuItem>
            <MenuItem value={"Plant-Based Diet"}> Plant-Based Diet</MenuItem>
            <MenuItem value={"Raw Food Diet"}> Raw Food Diet</MenuItem>
            <MenuItem value={"Ketogenic Diet"}> Ketogenic Diet</MenuItem>
            <MenuItem value={"Paleo Diet"}> Paleo Diet</MenuItem>
            <MenuItem value={"Flexitarian Diet"}> Flexitarian Diet</MenuItem>
            <MenuItem value={"Mediterranean Diet"}> Mediterranean Diet</MenuItem>
            <MenuItem value={"Dash Diet"}> Dash Diet</MenuItem>
            <MenuItem value={"Hazelnut Allergy"}> Hazelnut Allergy</MenuItem>
            <MenuItem value={"Macadamia Nut Allergy"}> Macadamia Nut Allergy</MenuItem>
            <MenuItem value={"Pistashio Allergy"}> Pistashio Allergy</MenuItem>
            <MenuItem value={"Cherry Allergy"}> Cherry Allergy</MenuItem>
            <MenuItem value={"Almond Allergy"}> Almond Allergy</MenuItem>
            <MenuItem value={"Walnut Allergy"}> Walnut Allergy</MenuItem>
            <MenuItem value={"Pecan Allergy"}> Pecan Allergy</MenuItem>
            <MenuItem value={"Cashew Allergy"}> Cashew Allergy</MenuItem>
            <MenuItem value={"Fig Allergy"}> Fig Allergy</MenuItem>
            <MenuItem value={"Date Allergy"}> Date Allergy</MenuItem>
            <MenuItem value={"Grape Allergy"}> Grape Allergy</MenuItem>
        </Select>
      </FormControl>
      <FormControl sx={{ m: 1, minWidth: 150 }}>
      <InputLabel id="demo-simple-select-helper-label"sx={{ color: 'white', text: '40px' }}>Cuisine</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={cuisine}
          label="Cuisine"
          onChange={handleCuisineFilter}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Bubble Tea"}>Bubble Tea</MenuItem>
          <MenuItem value={"Coffee & Tea"}>Coffee & Tea</MenuItem>
          <MenuItem value={"American"}>American</MenuItem>
          <MenuItem value={"Food Trucks"}>Food Trucks</MenuItem>
          <MenuItem value={"Sushi Bars"}>Sushi Bars</MenuItem>
          <MenuItem value={"Vegan"}>Vegan</MenuItem>
          <MenuItem value={"Pizza"}>Pizza</MenuItem>
          <MenuItem value={"Diners"}>Diners</MenuItem>
          <MenuItem value={"Gluten-Free"}>Gluten-Free</MenuItem>
          <MenuItem value={"Japanese"}>Japanese</MenuItem>
          <MenuItem value={"Seafood"}>Seafood</MenuItem>
          <MenuItem value={"Mediterranean"}>Mediterranean</MenuItem>
          <MenuItem value={"Chocolatiers & Shops"}>Chocolatiers & Shops</MenuItem>
          <MenuItem value={"Desserts"}>Desserts</MenuItem>
          <MenuItem value={"Cocktail Bars"}>Cocktail Bars</MenuItem>
          <MenuItem value={"American"}>American</MenuItem>
          <MenuItem value={"Allergists"}>Allergists</MenuItem>
          <MenuItem value={"Southern"}>Southern</MenuItem>
          <MenuItem value={"Bakeries"}>Bakeries</MenuItem>
          <MenuItem value={"Filipino"}>Filipino</MenuItem>
          <MenuItem value={"Noodles"}>Noodles</MenuItem>
          <MenuItem value={"Dim Sum"}>Dim Sum</MenuItem>
          <MenuItem value={"Beer Wine & Spirits"}>Beer Wine & Spirits</MenuItem>
          <MenuItem value={"Hospitals"}>Hospitals</MenuItem>
          <MenuItem value={"Bars"}>Bars</MenuItem>
          <MenuItem value={"Sandwiches"}>Sandwiches</MenuItem>
          <MenuItem value={"Vegetarian"}>Vegetarian</MenuItem>
          <MenuItem value={"Burgers"}>Burgers</MenuItem>
          <MenuItem value={"Bagels"}>Bagels</MenuItem>
          <MenuItem value={"Wine Bars"}>Wine Bars</MenuItem>
          <MenuItem value={"Tapas/Small Plates"}>Tapas/Small Plates</MenuItem>
          <MenuItem value={"Steakhouses"}>Steakhouses</MenuItem>
          <MenuItem value={"Lounges"}>Lounges</MenuItem>
          <MenuItem value={"Candy Stores"}>Candy Stores</MenuItem>
          <MenuItem value={"Gift Shops"}>Gift Shops</MenuItem>
          <MenuItem value={"Asian Fusion"}>Asian Fusion</MenuItem>
          <MenuItem value={"Sports Bars"}>Sports Bars</MenuItem>
          <MenuItem value={"New American"}>New American</MenuItem>
          <MenuItem value={"Vietnamese"}>Vietnamese</MenuItem>
          <MenuItem value={"Chinese"}>Chinese</MenuItem>
          <MenuItem value={"Emergency Rooms"}>Emergency Rooms</MenuItem>
          <MenuItem value={"Tacos"}>Tacos</MenuItem>
          <MenuItem value={"Korean"}>Korean</MenuItem>
          <MenuItem value={"Barbeque"}>Barbeque</MenuItem>
          <MenuItem value={"French"}>French</MenuItem>
          <MenuItem value={"Creperies"}>Creperies</MenuItem>
          <MenuItem value={"Indian"}>Indian</MenuItem>
          <MenuItem value={"Food Delivery Services"}>Food Delivery Services</MenuItem>
          <MenuItem value={"Thai"}>Thai</MenuItem>
          <MenuItem value={"Italian"}>Italian</MenuItem>
          <MenuItem value={"Fast Food"}>Fast Food</MenuItem>
          <MenuItem value={"Cheese Shops<"}>Cheese Shops</MenuItem>
          <MenuItem value={"Delis"}>Delis</MenuItem>
          <MenuItem value={"Patisserie/Cake Shop"}>Patisserie/Cake Shop</MenuItem>
          <MenuItem value={"Grocery"}>Grocery</MenuItem>
          <MenuItem value={"Organic Stores"}>Organic Stores</MenuItem>
          <MenuItem value={"Venues & Event Spaces"}>Venues & Event Spaces</MenuItem>
          <MenuItem value={"Specialty Food"}>Specialty Food</MenuItem>
          <MenuItem value={"Pediatricians"}>Pediatricians</MenuItem>
          <MenuItem value={"Gastroenterologist"}>Gastroenterologist</MenuItem>
          <MenuItem value={"Internal Medicine"}>Internal Medicine</MenuItem>
          <MenuItem value={"Bagels"}>Bagels</MenuItem>
          <MenuItem value={"Salad"}>Salad</MenuItem>
          <MenuItem value={"Pubs"}>Pubs</MenuItem>
          <MenuItem value={"Comfort Food"}>Comfort Food</MenuItem>
          <MenuItem value={"Cafes"}>Cafes</MenuItem>
          <MenuItem value={"Ethiopian"}>Ethiopian</MenuItem>
          <MenuItem value={"Beer Bar"}>Beer Bar</MenuItem>
          <MenuItem value={"Gastropubs"}>Gastropubs</MenuItem>
          <MenuItem value={"Hawaiian"}>Hawaiian</MenuItem>
          <MenuItem value={"Poke"}>Poke</MenuItem>
          <MenuItem value={"Acai Bowls"}>Acai Bowls</MenuItem>
          <MenuItem value={"Weight Loss Centers"}>Weight Loss Centers</MenuItem>
          <MenuItem value={"Body Contouring"}>Body Contouring</MenuItem>
          <MenuItem value={"Neurologist"}>Neurologist</MenuItem>
          <MenuItem value={"Counseling & Mental Health"}>Counseling & Mental Health</MenuItem>
          <MenuItem value={"Personal Chefs"}>Personal Chefs</MenuItem>
          <MenuItem value={"CSA"}>CSA</MenuItem>
          <MenuItem value={"Turkish"}>Turkish</MenuItem>
          <MenuItem value={"Greek"}>Greek</MenuItem>
          <MenuItem value={"International Grocery"}>International Grocery</MenuItem>
          <MenuItem value={"Lebanese"}>Lebanese</MenuItem>
          <MenuItem value={"Halal"}>Halal</MenuItem>
          <MenuItem value={"Trainers"}>Trainers</MenuItem>
          <MenuItem value={"Gyms"}>Gyms</MenuItem>
          <MenuItem value={"Nutritionist"}>Nutritionist</MenuItem>
          <MenuItem value={"Juice Bars & Smoothies"}>Juice Bars & Smoothies</MenuItem>
          <MenuItem value={"Cupcakes"}>Cupcakes</MenuItem>
          <MenuItem value={"Gelato"}>Gelato</MenuItem>
          <MenuItem value={"Brazilian"}>Brazilian</MenuItem>
          <MenuItem value={"Laotian"}>Laotian</MenuItem>
          <MenuItem value={"Pulmonologist"}>Pulmonologist</MenuItem>
          <MenuItem value={"Internal Medicine"}>Internal Medicine</MenuItem>
          <MenuItem value={"Ear Nose & Throat"}>Ear Nose & Throat</MenuItem>
          <MenuItem value={"Sleep Specialists"}>Sleep Specialists</MenuItem>
          <MenuItem value={"Tobacco Shops"}>Tobacco Shops</MenuItem>
          <MenuItem value={"uffets"}>Buffets</MenuItem>
          <MenuItem value={"Health Markets"}>Health Markets</MenuItem>
          <MenuItem value={"Macarons"}>Macarons</MenuItem>
        </Select>
      </FormControl>

      <FormControl sx={{ m: 1, minWidth: 150 }}>
        <InputLabel id="demo-simple-select-helper-label" sx={{ color: 'white', text: '40px' }}>Distance</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={distance}
          label="Distance"
          onChange={handleDistanceFilter}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={5}>Within 5 miles</MenuItem>
          <MenuItem value={10}>Within 10 miles</MenuItem>
          <MenuItem value={20}>Within 20 miles</MenuItem>
          <MenuItem value={21}>Over 20 miles</MenuItem>
        </Select>
      </FormControl>

      <FormControl sx={{ m: 1, minWidth: 150 }}>
        <InputLabel id="demo-simple-select-helper-label" sx={{ color: 'white', text: '40px' }}>Rating</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={rating}
          label="Rating"
          onChange={handleRatingFilter}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={1}>1 star</MenuItem>
          <MenuItem value={2}>2 stars</MenuItem>
          <MenuItem value={3}>3 stars</MenuItem>
          <MenuItem value={4}>4 stars</MenuItem>
          <MenuItem value={5}>5 stars</MenuItem>
        </Select>
      </FormControl>     
    </div>
      <Box textAlign='center' sx={{ marginTop: '-15px', marginBottom: '25px' }}>
    <Button variant="contained" sx={{ marginLeft: "10px", bgcolor: "#3498db", color: "#ffffff" }} onClick={handleReset}>
      Reset
    </Button>
  </Box>
      <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
        {data.map((restaurant, index) => (
          <div key={index} style={{ width: "30%", marginBottom: "20px" }}>
            <RestaurantCard restaurant={restaurant} searchTerm = {searchQuery} />
          </div>
        ))}
        
      </div>
      <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
        <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
          Results: {Math.min(pageNum * perPage, numInstances)} of {numInstances}
        </Typography>
      </div>
      <div style={{ textAlign: 'center', marginBottom: '20px' }}>
        <button
          onClick={() => setPageNum((prev) => prev - 1)}
          disabled={pageNum === 1}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum > 1 ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginRight: '10px'
          }}
        >
          Previous
        </button>

        {[...Array(Math.ceil(numInstances / perPage)).keys()].map((pageNumber) => (
          <button
            key={pageNumber + 1}
            onClick={() => setPageNum(pageNumber + 1)}
            style={{
              padding: '8px 16px',
              backgroundColor: pageNum === pageNumber + 1 ? 'green' : 'gray',
              color: 'white',
              border: 'none',
              borderRadius: '4px',
              cursor: 'pointer',
              outline: 'none',
              marginRight: '5px'
            }}
          >
            {pageNumber + 1}
          </button>
        ))}

        <button
          onClick={() => {
            if (data.length > 0) {
              setPageNum((prev) => prev + 1);
            }
          }}
          disabled={pageNum === Math.ceil(numInstances / perPage)}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum < Math.ceil(numInstances / perPage) ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginLeft: '10px'
          }}
        >
          Next
        </button>
      </div>
    </>
  );
};
export { restaurantAPIData };