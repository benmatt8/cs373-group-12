import React from 'react';
import { Link } from 'react-router-dom';
import Card from '@mui/material/Card';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';

const RestaurantCard = ({ restaurant, searchTerm}) => {

  const getHighlightedText = (text, highlight) =>{
    // Split text on highlight term, include term itself into parts, ignore case
    const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
    return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <b style={{backgroundColor: "#87876B"}}>{part}</b> : part)}</span>;
};
  return (
    <Link to={`/restaurants/${restaurant.name}`} state={restaurant} style={{ textDecoration: 'none' }}>
      <Card
        style={{
          width: "100%",
          height: "90%",
          marginBottom: "80px",
          boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
          backgroundColor: "#636684",
          color: "#fff"
        }}
      >
        <CardMedia
          component="img"
          sx={{ height: 250, width: "100%" }}
          image={restaurant.image_url}
          alt="image"
          title={restaurant.name}
        />
        <CardContent>
        <Typography variant="body2" sx={{ fontSize: "23px", color: "#e5dfda", fontWeight: "bold", fontFamily: "Arial, sans-serif" }}>
            {getHighlightedText(restaurant.name, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda" marginTop= "10px">
            <strong>Address:</strong> {getHighlightedText(restaurant.display_address, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda">
            <strong>Distance from UT Austin:</strong> {restaurant.distance} miles
          </Typography>
          <Typography variant="body2" color="#e5dfda">
            <strong>Cuisine:</strong> {getHighlightedText(restaurant.cuisine, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda">
            <strong>Rating:</strong> {restaurant.rating}
          </Typography>
          <Typography variant="body2" color="#e5dfda">
            <strong>Allergens:</strong> {getHighlightedText(restaurant.allergens, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda">
            <strong>Price:</strong> {restaurant.price !== null ? restaurant.price : "N/A"}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  )    
};

export default RestaurantCard;
