import React from 'react';
import { Link, useLocation } from 'react-router-dom';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { FaStar, FaClock, FaCheckCircle, FaTimesCircle } from 'react-icons/fa';
import { useEffect, useState } from "react";
import CardMedia from '@mui/material/CardMedia';


export const RestaurantDetails = () => {
  const [similarData, setSimilarData] = useState(null);
  const [getSimilarData, setSimilarBool] = useState(true);
  const { state } = useLocation();
  window.scrollTo(0, 0);

  let mapUrl = `https://maps.google.com/maps?q=${state.latitude},${state.longitude}&z=15&output=embed`;
  const parsedReviews = JSON.parse(state.reviews);
  const parsedOpeningHours = state.opening_hours ? JSON.parse(state.opening_hours) : null;

  const fetchSimilarData = async (getSimilarData) => {
    try {
      const getRecipe = true;
      const getRestaurant = false;
      const getAllergy = true;
      const response = await fetch(`${window.backendHost}/similar?recipe=${getRecipe}&restaurant=${getRestaurant}&allergy=${getAllergy}`);
      if (response.ok) {
        const data = await response.json();
        setSimilarData(data.similar);

      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchSimilarData(getSimilarData);
  }, [getSimilarData]);

  return similarData && (
    <>
      <Grid container justifyContent="center" alignItems="flex-start" style={{ padding: '20px' }}>
        <Grid item xs={12} md={8} lg={6}>
          <Card variant="outlined" style={{ marginTop: '10px', backgroundColor: "#434765", color: "white"}}>
            <CardContent>
              <Typography variant="h4" align="center" gutterBottom>{state.name}</Typography>
              <Grid container spacing={2}>
                <Grid item xs={12} sm={6}>
                  <img src={state.image_url} alt={"couldn't load"} style={{ width: '100%', height: 'auto', borderRadius: '8px' }} />
                </Grid>
                <Grid item xs={12} sm={6}>
                  {/* Specific information about the chosen restaurant */}
                  <Typography variant="body1"><strong>Location:</strong> {state.display_address}</Typography>
                  <Typography variant="body1"><strong>Distance from UT Austin:</strong> {state.distance}</Typography>
                  <Typography variant="body1"><strong>Cuisine:</strong> {state.cuisine}</Typography>
                  <Typography variant="body1"><strong>Rating:</strong> {state.rating}</Typography>
                  <Typography variant="body1">
                    {/*<Link to={`/allergyInfo/${restaurant.allId}`}> */}
                      View Allergens: {state.allergens}
                    {/* </Link> */}
                  </Typography>
                  <Typography variant="body1"><strong>Price:</strong> {state.price}</Typography>
                  <Typography variant="body1"><strong>Contact Number:</strong> {state.display_phone}</Typography>
                  <Typography variant="body1">
                    <strong>Status:</strong> {state.is_closed ? <FaTimesCircle style={{ color: 'red', verticalAlign: 'middle' }} /> : <FaCheckCircle style={{ color: 'green', verticalAlign: 'middle' }} />} {state.is_closed ? "Closed" : "Open Now"}
                  </Typography>
                </Grid>
              </Grid>

              {/* Hours Open section */}
              <Typography variant="h5" style={{ marginTop: '20px' }}>
                <strong>Hours Open</strong>
              </Typography>

              {/* Display opening hours */}
              <Grid container spacing={1} justifyContent="center">
                {parsedOpeningHours !== null && parsedOpeningHours.length > 0 ? (
                  parsedOpeningHours.map((hrs, index) => (
                    <Grid key={index} item xs={12} sm={6} style={{ display: 'flex', alignItems: 'center', height: '50px', paddingLeft: '50px' }}>
                      <FaClock style={{ marginRight: '5px', fontSize: '20px' }} />
                      <Typography variant="body2">
                        <strong>{hrs.day}:</strong> {hrs.start} - {hrs.end}
                      </Typography>
                    </Grid>
                  ))
                ) : (
                  <Grid item xs={12}>
                    <Typography variant="body2" style={{ marginTop: '20px', textAlign: 'center' }}>
                      Hours currently unavailable
                    </Typography>
                  </Grid>
                )}

                {/* Add an empty grid item if there is an odd number of days */}
                {parsedOpeningHours.length % 2 !== 0 && (
                  <Grid item xs={12} sm={6} style={{ display: 'flex', alignItems: 'center', height: '50px', paddingLeft: '50px' }}>
                    {/* You can put any content you want here */}
                  </Grid>
                )}
              </Grid>
            </CardContent>
          </Card>

          {/* Map with spacing */} 
          <div style={{ marginTop: '20px' }}></div>
          <iframe
            title="Restaurant Location"
            width="100%"
            height="400"
            frameBorder="0"
            src={mapUrl}
            allowFullScreen
            loading="lazy"
          ></iframe>

          {/* Reviews section */}
          <Typography variant="h5" style={{ marginTop: '20px', color:"#e5dfda"}}>Reviews</Typography>
          {parsedReviews.map((review, index) => (
            <Card key={index} variant="outlined" style={{ marginTop: '10px',  backgroundColor: "#434765", color: "white" }}>
              <CardContent>
                <Typography variant="subtitle1"><strong>{review.reviewer}</strong></Typography>
                <Typography variant="body2">{review.text}</Typography>
                <div>
                  {[...Array(review.rating)].map((_, i) => (
                    <span key={i}><FaStar style={{ color: 'yellow', marginRight: '5px' }} /></span>
                  ))}
                </div>
              </CardContent>
            </Card>
          ))}
        </Grid>
      </Grid>

      {/* Instances this instance links to */}
      <Typography variant="h5" style={{ marginTop: '50px', textAlign: 'center', color: "#e5dfda" }}>
        Similar pages you might be interested in:
      </Typography>

      <div style={{ display: 'flex', justifyContent: 'space-between', margin: '20px auto', maxWidth: '1200px' }}>
        <Link to={`/allergyInfo/${similarData.allergy.id}`} state={similarData.allergyInfo} style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#e5dfda"
          }}>
            <CardMedia
              component="img"
              height="500"
              image={require(`${similarData.allergy.imageUrl}`)}
              alt="image"
            />
            <CardContent>
              <Typography variant="body2" sx={{ fontSize: "23px", color:"#e5dfda"}}>
                {similarData.allergy.title}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Kind:</strong> {similarData.allergy.title} <br />
                <strong>Severity:</strong> {similarData.allergy.severity} <br />
                <strong>Prevalence:</strong> {similarData.allergy.prevalence} <br />
                <strong>Allergy:</strong> {similarData.allergy.allergy ? 'Yes' : 'No'} <br />
                <strong>Lifestyle:</strong> {similarData.allergy.lifestyle ? 'Yes' : 'No'} <br />
                <strong>Triggers:</strong> {similarData.allergy.triggers} <br />
                <strong>Affected Age Group:</strong> {similarData.allergy.commonAgeGroup} <br />
              </Typography>
            </CardContent>
          </Card>
        </Link>

        <Link to={`/recipes/${similarData.recipe.videoId}`} state={similarData.recipe} style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#fff"
          }}>
            <CardMedia
              component="img"
              height="500"
              image={similarData.recipe.imageUrl}
              alt="image"
            />
            <CardContent>
              <Typography variant="body2" sx={{ fontSize: "23px", color: "#e5dfda" }}>
                {similarData.recipe.title}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Channel:</strong> {similarData.recipe.channelTitle}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Description:</strong> {similarData.recipe.description.substring(0, 30)}...
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Published:</strong> {similarData.recipe.publishTime.substring(0, 10)}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Views:</strong> {similarData.recipe.viewCount}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Likes:</strong> {similarData.recipe.likeCount}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Total Comments:</strong> {similarData.recipe.commentCount}
              </Typography>
            </CardContent>
          </Card>
        </Link>
      </div>
    </>
  );
};
