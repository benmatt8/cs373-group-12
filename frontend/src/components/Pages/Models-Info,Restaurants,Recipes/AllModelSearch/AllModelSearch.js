import React, { useState, useEffect } from 'react';
import { useLocation, useParams } from 'react-router-dom';
import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';
import Typography from '@mui/material/Typography';
import { Tabs, Tab, Button } from '@mui/material';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import RestaurantCard from "../Restaurants/RestaurantCard";
import RecipeCard from "../Recipes/RecipeCard";
import AllergyCard from '../AllergyInfo/AllergyInfoCard';

export const AllModelSearch = () => {
  const {searchTerm} = useParams();
  // const [query, setQuery] = useState(''); 

  const [restaurants, setRestaurants] = useState([]);  
  const [restaurantsMeta, setMetaRestaurants] = useState(0);
  
  const [recipes, setRecipes] = useState([]);
  const [recipesMeta, setMetaRecipes] = useState(0);

  const [allergies, setAllergies] = useState([]);
  const [allergiesMeta, setMetaAllergies] = useState(0);

  const [restaurantPageNum, setRestaurantPageNum] = useState(1)
  const [allergyPageNum, setAllergyPageNum] = useState(1)
  const [recipePageNum, setRecipePageNum] = useState(1)
  
  const [selectedTab, setSelectedTab] = useState(0);
  
  const fetchRecipeData = async (pageNum, searchQuery) => {
    try {
      let response = '';
      if (searchQuery != '') {
        response = await fetch(`${window.backendHost}/search/recipes/${searchQuery}?page=${pageNum}&per_page=${6}`);
      }
    
      if (response.ok) {
        const data = await response.json();
        setRecipes(data.recipes);
        setMetaRecipes(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const fetchRestaurantData = async (pageNum, searchQuery) => {
    try {
      let response = '';
      if (searchQuery != '') {
        // if empty string, take back to home page
        response = await fetch(`${window.backendHost}/search/restaurants/${searchQuery}?page=${pageNum}&per_page=${6}`);
      }

      if (response.ok) {
        const data = await response.json();
        setRestaurants(data.restaurants);
        setMetaRestaurants(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const fetchAllergyData = async (pageNum, searchQuery) => {
    try {
      let response = '';
      if (searchQuery != '') {
        // if empty string, take back to home page
        response = await fetch(`${window.backendHost}/search/allergyInfo/${searchQuery}?page=${pageNum}&per_page=${6}`);
      }

      if (response.ok) {
        const data = await response.json();
        console.log("Data received from server:", data); // Add this line to log the received data
        setAllergies(data.allergyInfo);
        setMetaAllergies(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  const handleTabChange = (event, newValue) => {
    setSelectedTab(newValue);
  };


  useEffect(() => {
    // const queryFromState = searchTerm;
    if (searchTerm) {
      // console.log('Query from state:', searchTerm);
      // setQuery(searchTerm);
      fetchAllergyData(allergyPageNum, searchTerm)
      fetchRestaurantData(restaurantPageNum, searchTerm)
      fetchRecipeData(recipePageNum , searchTerm)
    }
  }, [searchTerm, selectedTab, allergyPageNum, restaurantPageNum, recipePageNum]);
  
  const getTabContent = () => {
    switch (selectedTab) {
      case 0:
        if (allergiesMeta == 0) {
          return (
            <h3 style={{ fontFamily: "cursive", fontSize: "2rem", textShadow: "2px 2px 4px #000", color: "red", marginTop: "50px" }}>No Results 😫 </h3>
          );
        }
        return (
          <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
            {allergies.map((Allergy, index) => (
              <AllergyCard key={index} allergy={Allergy} searchTerm = {searchTerm} />
            ))}
          </div>
        );
      case 1:
        if (restaurantsMeta == 0) {
          return (
            <h3 style={{ fontFamily: "cursive", fontSize: "2rem", textShadow: "2px 2px 4px #000", color: "red", marginTop: "50px" }}>No Results 😫</h3>
          );
        }
        return (
          <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
            {restaurants.map((restaurant, index) => (
              <div key={index} style={{ width: "30%", marginBottom: "20px" }}>
                <RestaurantCard restaurant={restaurant} searchTerm = {searchTerm} />
              </div>
            ))}
          </div>
        );
      case 2:
        if (recipesMeta == 0) {
          return (
            <h3 style={{ fontFamily: "cursive", fontSize: "2rem", textShadow: "2px 2px 4px #000", color: "red", marginTop: "50px" }}>No Results 😫</h3>
          );
        }
        return (
          <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
            {recipes.map((recipe, index) => (
              <RecipeCard key={index} recipe={recipe} searchTerm = {searchTerm}/>
            ))}
          </div>
        );
      default:
        return null;
    }
  };

  const getPagination = () => {
    switch (selectedTab) {
      case 0: 
        return (
          <>
            <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
              <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
                Results: {Math.min(allergyPageNum * 6, allergiesMeta)} of {allergiesMeta}
              </Typography>
            </div>
            <div style={{ textAlign: 'center', marginBottom: '20px' }}>
              <button
                onClick={() => setAllergyPageNum((prev) => prev - 1)}
                disabled={allergyPageNum === 1}
                style={{
                  padding: '8px 16px',
                  backgroundColor: allergyPageNum > 1 ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginRight: '10px'
                }}
              >
                Previous
              </button>

              {[...Array(Math.ceil(allergiesMeta / 6)).keys()].map((pageNumber) => (
                <button
                  key={pageNumber + 1}
                  onClick={() => setAllergyPageNum(pageNumber + 1)}
                  style={{
                    padding: '8px 16px',
                    backgroundColor: allergyPageNum === pageNumber + 1 ? 'green' : 'gray',
                    color: 'white',
                    border: 'none',
                    borderRadius: '4px',
                    cursor: 'pointer',
                    outline: 'none',
                    marginRight: '5px'
                  }}
                >
                  {pageNumber + 1}
                </button>
              ))}

              <button
                onClick={() => {
                }}
                disabled={allergyPageNum === Math.ceil(allergiesMeta / 6)}
                style={{
                  padding: '8px 16px',
                  backgroundColor: allergyPageNum < Math.ceil(allergiesMeta / 6) ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginLeft: '10px'
                }}
              >
                Next
              </button>
            </div>
          </>
        );
      case 1:
        return (
          <>
            <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
              <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
                Results: {Math.min(restaurantPageNum * 6, restaurantsMeta)} of {restaurantsMeta}
              </Typography>
            </div>
            <div style={{ textAlign: 'center', marginBottom: '20px' }}>
              <button
                onClick={() => setRestaurantPageNum((prev) => prev - 1)}
                disabled={restaurantPageNum === 1}
                style={{
                  padding: '8px 16px',
                  backgroundColor: restaurantPageNum > 1 ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginRight: '10px'
                }}
              >
                Previous
              </button>

              {[...Array(Math.ceil(restaurantsMeta / 6)).keys()].map((pageNumber) => (
                <button
                  key={pageNumber + 1}
                  onClick={() => setRestaurantPageNum(pageNumber + 1)}
                  style={{
                    padding: '8px 16px',
                    backgroundColor: restaurantPageNum === pageNumber + 1 ? 'green' : 'gray',
                    color: 'white',
                    border: 'none',
                    borderRadius: '4px',
                    cursor: 'pointer',
                    outline: 'none',
                    marginRight: '5px'
                  }}
                >
                  {pageNumber + 1}
                </button>
              ))}

              <button
                onClick={() => {
                }}
                disabled={restaurantPageNum === Math.ceil(restaurantsMeta / 6)}
                style={{
                  padding: '8px 16px',
                  backgroundColor: restaurantPageNum < Math.ceil(restaurantsMeta / 6) ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginLeft: '10px'
                }}
              >
                Next
              </button>
            </div>
          </>
        );
      case 2:
        return (
          <>
            <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
              <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
                Results: {Math.min(recipePageNum * 6, recipesMeta)} of {recipesMeta}
              </Typography>
            </div>
            <div style={{ textAlign: 'center', marginBottom: '20px' }}>
              <button
                onClick={() => setRecipePageNum((prev) => prev - 1)}
                disabled={recipePageNum === 1}
                style={{
                  padding: '8px 16px',
                  backgroundColor: recipePageNum > 1 ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginRight: '10px'
                }}
              >
                Previous
              </button>

              {[...Array(Math.ceil(recipesMeta / 6)).keys()].map((pageNumber) => (
                <button
                  key={pageNumber + 1}
                  onClick={() => setRecipePageNum(pageNumber + 1)}
                  style={{
                    padding: '8px 16px',
                    backgroundColor: recipePageNum === pageNumber + 1 ? 'green' : 'gray',
                    color: 'white',
                    border: 'none',
                    borderRadius: '4px',
                    cursor: 'pointer',
                    outline: 'none',
                    marginRight: '5px'
                  }}
                >
                  {pageNumber + 1}
                </button>
              ))}

              <button
                onClick={() => {
                }}
                disabled={recipePageNum === Math.ceil(recipesMeta / 6)}
                style={{
                  padding: '8px 16px',
                  backgroundColor: recipePageNum < Math.ceil(recipesMeta / 6) ? 'green' : 'gray',
                  color: 'white',
                  border: 'none',
                  borderRadius: '4px',
                  cursor: 'pointer',
                  outline: 'none',
                  marginLeft: '10px'
                }}
              >
                Next
              </button>
            </div>
          </>
        );
    }
  }

  return (
    <>
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", paddingBottom: "20px" }}>
        <h1 style={{ fontFamily: "cursive", fontSize: "3rem", textShadow: "2px 2px 4px #000", color: "#7CB342" }}>All Results</h1>
        <Tabs value={selectedTab} onChange={handleTabChange}>
          <Tab label="Allergies" value={0} sx={{ color: 'white' }} />
          <Tab label="Restaurants" value={1} sx={{ color: 'white' }} />
          <Tab label="Recipes" value={2} sx={{ color: 'white' }} />
        </Tabs>
      </div>
      <div>{getTabContent()}</div>
      <div>{getPagination()}</div>
    </>
  );
};

