import React, { useState, useEffect, useRef } from "react";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';
import Button from '@mui/material/Button';
import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';
import Box from '@mui/material/Box';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import InputLabel from '@mui/material/InputLabel';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { FaSearch } from "react-icons/fa";
import { FaCaretDown } from "react-icons/fa";
import RecipeCard from "./RecipeCard";


// pulled from youtube api, stores data about recipe videos
const recipeAPIData = [];

export const Recipes = () => {
  const [allergy, setAllergy] = React.useState('');
  const [order, setOrder] = useState('');
  const [sortBy, setSortBy] = useState('');
  
  const [searchQuery, setSearchQuery] = useState('');

  const [data, setData] = useState([]);
  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(6); 
  const [numInstances, setMetaData] = useState(1);
  const [sortMenuAnchor, setSortMenuAnchor] = useState(null);



  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSearch();
    }
  };

  const fetchData = async (pageNum, allergy, order, sortBy, searchQuery) => {
    try {
      let response = '';
      if (searchQuery === '') {
        response = await fetch(`${window.backendHost}/recipes?page=${pageNum}&per_page=${perPage}&allergy=${allergy}&order=${order}&sortBy=${sortBy}`);
      } else {
        response = await fetch(`${window.backendHost}/search/recipes/${searchQuery}?page=${pageNum}&per_page=${perPage}`);
      }
     
      if (response.ok) {
        const data = await response.json();
        setData(data.recipes);
        setMetaData(data.meta.count);
        window.scrollTo(0, 0);
      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };


  useEffect(() => {
    fetchData(pageNum, allergy, order, sortBy, searchQuery);
  }, [pageNum, allergy, order, sortBy, searchQuery]);

  const handleAllergyFilter = (event) => {
    setAllergy(event.target.value);
  };


  const handleOrder = (event) => {
    setOrder(event.target.value);
  };

  const handleSortByCriteria = (criteria) => {
    setSortBy(criteria);
  }; 

  const inputRef = useRef(null);
  const handleSearch = () => {
    console.log(inputRef.current.value);
    setSearchQuery(inputRef.current.value);
  };

  const handleReset = () => {
    setOrder('');
    setSortBy('');
    setAllergy(''); 
    setSearchQuery('');
  };


  return (
    <>
    <h1 style={{ fontFamily: "cursive", fontSize: "3rem", textShadow: "2px 2px 4px #000", color: "#7CB342" }}>Recipes</h1>
    {/* change for search bar */}
    <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center' }}>
  <input
    type="text"
    placeholder="Search..."
    ref={inputRef}
    style={{
      marginBottom: '10px',
      padding: '6px', /* Decreased padding for a smaller size */
      width: '25%', /* Reduced width for a smaller size */
      height: '25px', /* Reduced height for a smaller size */
      borderRadius: '12px', /* Rounded corners for a modern look */
      border: '2px solid #3498db', /* Changed border color */
      boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)', /* Added box shadow for depth */
      backgroundColor: '#ADD8E6' /* Pastel light green color */
    }}
    onKeyPress={handleKeyPress}
  />
  <button
    onClick={handleSearch}
    style={{
      backgroundColor: '#3498db', /* Changed background color */
      border: 'none', /* Removed border for cleaner appearance */
      height: '35px', /* Matched button height with input */
      width: '30px',
      marginLeft: '3px', /* Adjusted margin for alignment */
      padding: '6px', /* Decreased padding for a smaller size */
      borderRadius: '12px', /* Rounded corners for button */
      cursor: 'pointer', /* Cursor pointer for indicating interactivity */
      marginTop: '2.5px' /* Adjusted marginTop to move the button up */
    }}
  >
    <FaSearch color="white" size={12} /> {/* Reduced icon size for a smaller size */}
  </button>
</div>




    <Box sx={{ display: "flex", justifyContent: "center", color: "white" }}>
        {/* Sort By Button */}
        <Button
          id="basic-button"
          variant="outlined"
          onClick={(event) => setSortMenuAnchor(event.currentTarget)} 
        >
          Sort By {sortBy} <FaCaretDown />
        </Button>
        <Menu
          id="basic-menu"
          anchorEl={sortMenuAnchor}
          open={Boolean(sortMenuAnchor)}
          onClose={() => setSortMenuAnchor(null)}
          MenuListProps={{
            "aria-labelledby": "basic-button",
          }}
        >
          <MenuItem onClick={() => handleSortByCriteria('')}><em>None</em></MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('title')}>Recipe Name</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('viewCount')}>View Count</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('likeCount')}>Like Count</MenuItem>
          <MenuItem onClick={() => handleSortByCriteria('commentCount')}>Comment Count</MenuItem>
        </Menu>
        <ToggleButtonGroup
          exclusive
          onChange={handleOrder}
          aria-label="sort order"
        >
          <ToggleButton
            value="low"
            aria-label="asc"
            sx={{
              color: 'white',
              backgroundColor: order === 'low' ? '#AEC6CF' : 'transparent', 
              '&:hover': {
                backgroundColor: order === 'low' ? '#AEC6CF' : 'gray', 
              },
            }}
          >
            Ascending
          </ToggleButton>
          <ToggleButton
            value="high"
            aria-label="desc"
            sx={{
              color: 'white',
              backgroundColor: order === 'high' ? '#AEC6CF' : 'transparent',
              '&:hover': {
                backgroundColor: order === 'high' ? '#AEC6CF' : 'gray',
              },
            }}
          >
            Descending
          </ToggleButton>
        </ToggleButtonGroup>
      </Box>
      <div>
      <FormControl sx={{ m: 1, minWidth: 150 }}>
        <InputLabel id="demo-simple-select-helper-label"sx={{ color: 'white', text: '40px' }}>Allergy</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          value={allergy}
          label="Allergy"
          onChange={handleAllergyFilter}
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={"Milk Allergy"}> Milk Allergy</MenuItem>
          <MenuItem value={"Shellfish Allergy"}> Shellfish Allergy</MenuItem>
          <MenuItem value={"Soy Allergy"}> Soy Allergy</MenuItem>
          <MenuItem value={"Eggs Allergy"}> Eggs Allergy</MenuItem>
          <MenuItem value={"Fish Allergy"}> Fish Allergy</MenuItem>
          <MenuItem value={"Crustacean Shellfish Allergy"}> Crustacean Shellfish Allergy</MenuItem>
          <MenuItem value={"Tree Nuts Allergy"}> Tree Nuts Allergy</MenuItem>
          <MenuItem value={"Peanuts Allergy"}> Peanuts Allergy</MenuItem>
          <MenuItem value={"Wheat Allergy"}> Wheat Allergy</MenuItem>
          <MenuItem value={"Sesame Allergy"}> Sesame Allergy</MenuItem>
          <MenuItem value={"Sulfite Allergy"}> Sulfite Allergy</MenuItem>
          <MenuItem value={"Mustard Allergy"}> Mustard Allergy</MenuItem>
          <MenuItem value={"Buckwheat Allergy"}> Buckwheat Allergy</MenuItem>
          <MenuItem value={"Celery Allergy"}> Celery Allergy</MenuItem>
          <MenuItem value={"Lupin Allergy"}> Lupin Allergy</MenuItem>
          <MenuItem value={"Mango Allergy"}> Mango Allergy</MenuItem>
          <MenuItem value={"Peach Allergy"}> Peach Allergy</MenuItem>
          <MenuItem value={"Pork Allergy"}> Pork Allergy</MenuItem>
          <MenuItem value={"Tomato Allergy"}> Tomato Allergy</MenuItem>
          <MenuItem value={"Strawberry Allergy"}> Strawberry Allergy</MenuItem>
          <MenuItem value={"Garlic Allergy"}> Garlic Allergy</MenuItem>
          <MenuItem value={"Oats Allergy"}> Oats Allergy</MenuItem>
          <MenuItem value={"Balsam of Peru Allergy"}> Balsam of Peru Allergy</MenuItem>
          <MenuItem value={"Maize Allergy"}> Maize Allergy</MenuItem>
          <MenuItem value={"Cinnamon Allergy"}> Cinnamon Allergy</MenuItem>
          <MenuItem value={"Meat Allergy"}> Meat Allergy</MenuItem>
          <MenuItem value={"Apple Allergy"}> Apple Allergy</MenuItem>
          <MenuItem value={"Gluten-Free Diet"}> Gluten-Free Diet</MenuItem>
          <MenuItem value={"Vegan Diet"}> Vegan Diet</MenuItem>
          <MenuItem value={"Vegetarian Diet"}> Vegetarian Diet</MenuItem>
          <MenuItem value={"Plant-Based Diet"}> Plant-Based Diet</MenuItem>
          <MenuItem value={"Raw Food Diet"}> Raw Food Diet</MenuItem>
          <MenuItem value={"Ketogenic Diet"}> Ketogenic Diet</MenuItem>
          <MenuItem value={"Paleo Diet"}> Paleo Diet</MenuItem>
          <MenuItem value={"Flexitarian Diet"}> Flexitarian Diet</MenuItem>
          <MenuItem value={"Mediterranean Diet"}> Mediterranean Diet</MenuItem>
          <MenuItem value={"Dash Diet"}> Dash Diet</MenuItem>
          <MenuItem value={"Hazelnut Allergy"}> Hazelnut Allergy</MenuItem>
          <MenuItem value={"Macadamia Nut Allergy"}> Macadamia Nut Allergy</MenuItem>
          <MenuItem value={"Pistashio Allergy"}> Pistashio Allergy</MenuItem>
          <MenuItem value={"Cherry Allergy"}> Cherry Allergy</MenuItem>
          <MenuItem value={"Almond Allergy"}> Almond Allergy</MenuItem>
          <MenuItem value={"Walnut Allergy"}> Walnut Allergy</MenuItem>
          <MenuItem value={"Pecan Allergy"}> Pecan Allergy</MenuItem>
          <MenuItem value={"Cashew Allergy"}> Cashew Allergy</MenuItem>
          <MenuItem value={"Fig Allergy"}> Fig Allergy</MenuItem>
          <MenuItem value={"Date Allergy"}> Date Allergy</MenuItem>
          <MenuItem value={"Grape Allergy"}> Grape Allergy</MenuItem>
        </Select>
      </FormControl>

    </div>
    <Box textAlign='center' sx={{ marginTop: '-15px', marginBottom: '25px' }}>
    <Button variant="contained" sx={{ marginLeft: "10px", bgcolor: "#3498db", color: "#ffffff" }} onClick={handleReset}>
      Reset
    </Button>
  </Box>
    <div style={{ display: "flex", justifyContent: "space-between", padding: "0 10px", flexWrap: "wrap" }}>
      {data.map((recipe, index) => (
        <RecipeCard key={index} recipe={recipe} searchTerm = {searchQuery}/>
      ))}
    </div>
    <div style={{ textAlign: 'center', marginBottom: '20px' }}>
        <div style={{ display: 'grid', placeItems: 'center', marginBottom: '30px' }}>
          <Typography variant="body2" sx={{ fontSize: "23px", color: "#afdff0" }}>
            Results: {Math.min(pageNum * perPage, numInstances)} of {numInstances}
          </Typography>
        </div>
        <button
          onClick={() => setPageNum((prev) => prev - 1)}
          disabled={pageNum === 1}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum > 1 ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginRight: '10px'
          }}
        >
          Previous
        </button>

        {[...Array(Math.ceil(numInstances / perPage)).keys()].map((pageNumber) => (
          <button
            key={pageNumber + 1}
            onClick={() => setPageNum(pageNumber + 1)}
            style={{
              padding: '8px 16px',
              backgroundColor: pageNum === pageNumber + 1 ? 'green' : 'gray',
              color: 'white',
              border: 'none',
              borderRadius: '4px',
              cursor: 'pointer',
              outline: 'none',
              marginRight: '5px'
            }}
          >
            {pageNumber + 1}
          </button>
        ))}

        <button
          onClick={() => {
            if (data.length > 0) {
              setPageNum((prev) => prev + 1);
            }
          }}
          disabled={pageNum === Math.ceil(numInstances / perPage)}
          style={{
            padding: '8px 16px',
            backgroundColor: pageNum < Math.ceil(numInstances / perPage) ? 'green' : 'gray',
            color: 'white',
            border: 'none',
            borderRadius: '4px',
            cursor: 'pointer',
            outline: 'none',
            marginLeft: '10px'
          }}
        >
          Next
        </button>
      </div>
    </>
  );
};
export { recipeAPIData }

