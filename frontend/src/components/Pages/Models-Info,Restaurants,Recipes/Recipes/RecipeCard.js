import React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { Link } from 'react-router-dom';

const RecipeCard = ({ recipe, searchTerm }) => {
  const getHighlightedText = (text, highlight) =>{
    // Split text on highlight term, include term itself into parts, ignore case
    const parts = text.split(new RegExp(`(${highlight})`, 'gi'));
    return <span>{parts.map(part => part.toLowerCase() === highlight.toLowerCase() ? <b style={{backgroundColor: "#87876B"}}>{part}</b> : part)}</span>;
};
  return (
    <Link to={`/recipes/${recipe.videoId}`} state={recipe} style={{ textDecoration: 'none', width: "30%", marginBottom: "80px", marginLeft: "20px", marginRight: "20px" }}>
      <Card
        style={{
          width: "100%",
          height: "90%",
          marginBottom: "80px",
          boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
          backgroundColor: "#636684",
          color: "#fff"
        }}
      >
        <CardMedia
          sx={{ height: 250, width: "100%" }}
          image={recipe.imageUrl}
          title={recipe.title}
        />
        <CardContent>
          <Typography variant="body2" sx={{ fontSize: "23px", color: "#e5dfda", fontWeight: "bold", fontFamily: "Arial, sans-serif" }}>
            {getHighlightedText(recipe.title, searchTerm)}
          </Typography>
          <Typography variant="body2" color="#e5dfda" sx={{ marginTop: "10px" }}>
            <strong>Channel:</strong> {getHighlightedText(recipe.channelTitle, searchTerm)} <br />
            <strong>Description:</strong> {getHighlightedText(recipe.description.substring(0, 200), searchTerm)}... <br />
            <strong>Published:</strong> {recipe.publishTime.substring(0, 10)} <br />
            <strong>Likes:</strong> {recipe.likeCount} <br />
            <strong>Views:</strong> {recipe.viewCount} <br />
            <strong>Comments:</strong> {recipe.commentCount} <br />
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
};

export default RecipeCard;
