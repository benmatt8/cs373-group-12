import React, { useState, useEffect } from "react";
import { useParams, useLocation } from "react-router-dom";
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import { Link } from 'react-router-dom';


export const RecipeDetails = () => {
  { /* Retrieves recipe parameters and data  */ }
  // const { id } = useParams();
  // const state = recipeAPIData.find(state => state.id.videoId === id);
  const [similarData, setSimilarData] = useState(null);
  const [getSimilarData, setSimilarBool] = useState(true);
  const { state } = useLocation();
  window.scrollTo(0, 0);

  const fetchSimilarData = async (getSimilarData) => {
    try {
      const getRecipe = false;
      const getRestaurant = true;
      const getAllergy = true;
      const response = await fetch(`${window.backendHost}/similar?recipe=${getRecipe}&restaurant=${getRestaurant}&allergy=${getAllergy}`);
      if (response.ok) {
        const data = await response.json();
        setSimilarData(data.similar);

      } else {
        console.error('Error fetching data:', response.status);
      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    fetchSimilarData(getSimilarData);
  }, [getSimilarData]);

  return similarData && (
    <>
      <Card style={{
        margin: '20px auto', maxWidth: '800px', backgroundColor: "#434765",
        color: "#fff"
      }} className="recipe-detail-container">
        <CardContent>
          <Typography variant="h5" component="h2" className="recipe-title">
            {state.title}
          </Typography>
          <iframe
            src={`https://www.youtube.com/embed/${state.videoId}`}
            frameborder='0'
            allow='autoplay; encrypted-media'
            allowfullscreen
            title='video'
            style={{ width: '100%', height: '500px', marginBottom: '20px' }}
          />
          <>
            {/* Displays more information about this recipe, video access */}
            <Typography variant="body1" className="recipe-detail">
              <strong>Channel:</strong> {state.channelTitle}
            </Typography>
            <Typography variant="body1" className="recipe-detail">
              <strong>Description:</strong> {state.description.substring(0, 100)}...
            </Typography>
            <Typography variant="body1" className="recipe-detail">
              <strong>Published:</strong> {state.publishTime.substring(0, 10)}
            </Typography>
            <Typography variant="body1" className="recipe-detail">
              <strong>Likes:</strong> {state.likeCount}
            </Typography>
            <Typography variant="body1" className="recipe-detail">
              <strong>Views:</strong> {state.viewCount}
            </Typography>
            <Typography variant="body1" className="recipe-detail">
              <strong>Comments:</strong> {state.commentCount}
            </Typography>
          </>
          <div className="image-container">
            <img
              src={state.imageUrl}
              alt="recipe"
              className="recipe-image"
              style={{ maxWidth: '100%', height: 'auto', borderRadius: '8px' }}
            />
          </div>
        </CardContent>
      </Card>
      
      { /* Cards that link to related instances */}
      <Typography variant="h5" style={{ marginTop: '50px', textAlign: 'center', color: "white" }}>
        Other pages you might be interested in:
      </Typography>
      <div style={{ display: 'flex', justifyContent: 'space-between', margin: '20px auto', maxWidth: '1200px' }}>
        <Link to={`/restaurants/${similarData.restaurant.name}`} state={similarData.restaurant} style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#fff"
          }}>
            <CardMedia
              component="img"
              height="400"
              image={similarData.restaurant.image_url}
              alt={similarData.restaurant.name}
            />
            <CardContent>
              <Typography gutterBottom variant="h5" component="div" color="#e5dfda">
                {similarData.restaurant.name}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Address:</strong> {similarData.restaurant.display_address}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Distance from UT Austin:</strong> {similarData.restaurant.distance}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Cuisine:</strong> {similarData.restaurant.cuisine}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Rating:</strong> {similarData.restaurant.rating}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Allergens:</strong> {similarData.restaurant.allergens}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Price:</strong> {similarData.restaurant.price}
              </Typography>
            </CardContent>
          </Card>
        </Link>

        <Link to={`/allergyInfo/${similarData.allergy.id}`} state={similarData.allergyInfo} style={{ textDecoration: 'none', width: '550px' }}>
          <Card style={{
            maxWidth: '550px', cursor: 'pointer', backgroundColor: "#434765",
            color: "#fff"
          }}>
            <CardMedia
              component="img"
              height="400"
              image={require(`${similarData.allergy.imageUrl}`)}
              alt={similarData.allergy.title}
            />
            <CardContent>
              <Typography variant="body2" sx={{ fontSize: "23px", color:"#e5dfda" }}>
                {similarData.allergy.title}
              </Typography>
              <Typography variant="body2" color="#e5dfda">
                <strong>Kind:</strong> {similarData.allergy.title} <br />
                <strong>Severity:</strong> {similarData.allergy.severity} <br />
                <strong>Prevalence:</strong> {similarData.allergy.prevalence} <br />
                <strong>Allergy:</strong> {similarData.allergy.allergy ? 'Yes' : 'No'} <br />
                <strong>Lifestyle:</strong> {similarData.allergy.lifestyle ? 'Yes' : 'No'} <br />
                <strong>Triggers:</strong> {similarData.allergy.triggers} <br />
                <strong>Affected Age Group:</strong> {similarData.allergy.commonAgeGroup} <br />
              </Typography>
            </CardContent>
          </Card>
        </Link>
      </div>
    </>
  );
}
