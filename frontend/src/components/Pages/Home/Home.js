import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Confetti from "react-confetti"; // Import react-confetti
import border from "./border.png";
import allergy_info_image from "./allergy_info_image.jpeg";
import restaurants_image from "./restaurants_image.jpeg";
import recipes_image from "./recipes_image.jpeg";
import { FaCaretDown } from "react-icons/fa";
import { Typography } from "@mui/material";


export const Home = () => {
  const [showBanner, setShowBanner] = useState(false);
  const [showConfetti, setShowConfetti] = useState(true);

  const useTypewriter = (text, speed = 100) => {
    const [displayText, setDisplayText] = useState("");

    useEffect(() => {
      let i = 0;
      const typingInterval = setInterval(() => {
        if (i < text.length) {
          setDisplayText((prevText) => prevText + text.charAt(i));
          i++;
        } else {
          clearInterval(typingInterval);
        }
      }, speed);

      return () => {
        clearInterval(typingInterval);
      };
    }, [text, speed]);

    return displayText.length === text.length ? text : displayText;
  };

  const Typewriter = ({ text, speed }) => {
    const displayText = useTypewriter(text, speed);
    return <p>{displayText}</p>;
  };

  useEffect(() => {
    const timeout = setTimeout(() => {
      setShowConfetti(false);
    }, 7000);

    return () => clearTimeout(timeout);
  }, []);

  return (
    <div
      style={{
        fontFamily: "Montserrat",
        fontSize: "24px",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "150vh",
      }}
    >
      {/* Render Confetti when showConfetti is true */}
      {showConfetti && (
        <Confetti
          wind={0.0}
          gravity={0.08}
          colors={["#434765", "tan", "green"]}
        />
      )}
      <div style={{ position: "relative", width: "100%", height: "auto", marginTop: '0px', }}>
        {/* Border photo */}
        <img
          src={border}
          alt="Border Photo"
          style={{
            width: "100%",
            height: "auto",
            display: "block",
            marginTop: '0px',
          }}
        />

        {/* Text */}
        <div
          style={{
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            textAlign: "center",
            width: "70%",

          }}
        >
          <h4
            style={{
              fontFamily: "Georgia serif",
              fontWeight: "thin",
              fontStyle: "italic",
              fontSize: "90px",
              color: "#e5dfda",
              marginTop: '0px',
              marginBottom: "10px",
            }}
          >
            <Typewriter text="Acchieving Food Freedom" />
          </h4>
          <h3> Helping Find The Perfect Fit For Your Dietary Needs</h3>
        </div>
      </div>

      {/* Pecha Kucha Link */}
      <div style={{
        backgroundColor: "#434765",
        marginTop: "60px",
        minHeight: "50vh",
        width: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
      }}>
        <div style={{
          marginTop: "30px",
          marginBottom: "15px",
          padding: "15px",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
        }}>
          <h4
            style={{
              fontSize: "20px",
              fontFamily: "Arial",
              color: "white",
              marginBottom: "5px",
              textAlign: "center",
            }}
          >Watch the video below to learn more about the issues surrounding dietary restrictions and our motivations for creating this website!
          </h4>
        </div>
        <iframe
          title="Safe Eats Pecha Kucha Video"
          width="560"
          height="315"
          src="https://www.youtube.com/embed/I96ZTBqviXc"
          frameborder="0"
          allowfullscreen
          style={{ marginLeft: "20px", marginBottom: "50px" }}
        ></iframe>
      </div>

      <div
        style={{
          backgroundColor: "#434765",
          minHeight: "60vh",
          width: "100%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          marginTop: "80px",
          marginBottom: "20px",
        }}
      >
        {/* Containers for images and buttons */}
        <div
          style={{
            width: "90%",
            display: "flex",
            flexDirection: "row",
            flexWrap: "wrap",
            justifyContent: "center",
            overflowY: "auto",
            marginBottom: "30px",
            marginTop: "40px",
          }}
        >
          <h5
            style={{
              fontSize: "20px",
              fontFamily: "Arial",
              color: "white",
              marginBottom: "20px",
              textAlign: "center",
            }}
          >
            Click below to explore further about allergies or discover dining and recipe selections tailored to your needs
          </h5>
          <FaCaretDown />

          <div
            style={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            {/* Card 1 */}
            <div
              style={{
                marginRight: "70px",
                textAlign: "center",
                backgroundColor: "#f5f5f5",
                borderRadius: "15px",
                padding: "30px",
                boxShadow: "0px 12px 20px rgba(0, 0, 0, 0.15)",
              }}
            >
              <div style={{ marginBottom: "20px" }}>
                <img
                  src={allergy_info_image}
                  alt="Allergy Info"
                  style={{
                    width: "250px",
                    height: "250px",
                    objectFit: "cover",
                    borderRadius: "15px",
                  }}
                />
              </div>
              <div>
                <Link
                  to="/allergyinfo"
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <button
                    style={{
                      backgroundColor: "#4488a4",
                      color: "white",
                      padding: "15px 30px",
                      border: "none",
                      borderRadius: "8px",
                      fontSize: "24px",
                      cursor: "pointer",
                      boxShadow: "0px 6px 12px rgba(0, 0, 0, 0.15)",
                      outline: "none",
                      textShadow: "2px 2px 4px rgba(0, 0, 0, 0.2)",
                    }}
                    onMouseEnter={(e) =>
                      (e.target.style.backgroundColor = "#2a5e6a")
                    }
                    onMouseLeave={(e) =>
                      (e.target.style.backgroundColor = "#3f7cac")
                    }
                  >
                    Allergy Info
                  </button>
                </Link>
              </div>
            </div>

            {/* Card 2 */}
            <div
              style={{
                marginRight: "70px",
                textAlign: "center",
                backgroundColor: "#f5f5f5",
                borderRadius: "15px",
                padding: "30px",
                boxShadow: "0px 12px 20px rgba(0, 0, 0, 0.15)",
              }}
            >
              <div style={{ marginBottom: "20px" }}>
                <img
                  src={restaurants_image}
                  alt="Restaurants"
                  style={{
                    width: "250px",
                    height: "250px",
                    objectFit: "cover",
                    borderRadius: "15px",
                  }}
                />
              </div>
              <div>
                <Link
                  to="/restaurants"
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <button
                    style={{
                      backgroundColor: "#4488a4",
                      color: "white",
                      padding: "15px 30px",
                      border: "none",
                      borderRadius: "8px",
                      fontSize: "24px",
                      cursor: "pointer",
                      boxShadow: "0px 6px 12px rgba(0, 0, 0, 0.15)",
                      outline: "none",
                      textShadow: "2px 2px 4px rgba(0, 0, 0, 0.2)",
                    }}
                    onMouseEnter={(e) =>
                      (e.target.style.backgroundColor = "#2a5e6a")
                    }
                    onMouseLeave={(e) =>
                      (e.target.style.backgroundColor = "#3f7cac")
                    }
                  >
                    Restaurants
                  </button>
                </Link>
              </div>
            </div>

            {/* Card 3 */}
            <div
              style={{
                textAlign: "center",
                backgroundColor: "#f5f5f5",
                borderRadius: "15px",
                padding: "30px",
                boxShadow: "0px 12px 20px rgba(0, 0, 0, 0.15)",
              }}
            >
              <div style={{ marginBottom: "20px" }}>
                <img
                  src={recipes_image}
                  alt="Recipes"
                  style={{
                    width: "250px",
                    height: "250px",
                    objectFit: "cover",
                    borderRadius: "15px",
                  }}
                />
              </div>
              <div>
                <Link
                  to="/recipes"
                  style={{ textDecoration: "none", color: "inherit" }}
                >
                  <button
                    style={{
                      backgroundColor: "#4488a4",
                      color: "white",
                      padding: "15px 30px",
                      border: "none",
                      borderRadius: "8px",
                      fontSize: "24px",
                      cursor: "pointer",
                      boxShadow: "0px 6px 12px rgba(0, 0, 0, 0.15)",
                      outline: "none",
                      textShadow: "2px 2px 4px rgba(0, 0, 0, 0.2)",
                    }}
                    onMouseEnter={(e) =>
                      (e.target.style.backgroundColor = "#2a5e6a")
                    }
                    onMouseLeave={(e) =>
                      (e.target.style.backgroundColor = "#3f7cac")
                    }
                  >
                    Recipes
                  </button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
     

    </div>
  );
};
