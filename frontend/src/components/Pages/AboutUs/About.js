import * as React from "react";
import Box from "@mui/material/Box";
import { Button } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { useState, useEffect } from "react";
import Link from "@mui/material/Link";
import { CardMedia } from "@mui/material";
import arushi from "./images/arushi.jpeg";
import nikhita from "./images/nikhita.png";
import anvi from "./images/anvi.png";
import ben from "./images/ben.png";
import jorge from "./images/jorge.png";
import reactlogo from "./images/react.png";
import gitlablogo from "./images/gitlab.png";
import materialuilogo from "./images/materialui.png";
import namecheaplogo from "./images/namecheap.png";
import visualstudiologo from "./images/visualstudio.png";
import postmanlogo from "./images/postman.png";
import pythonlogo from "./images/python.png";
import dockerlogo from "./images/docker.png";

// Team information
const teamInfo = [
  {
    name: "Arushi Sharma",
    gitlabName: "ArushiSharma",
    altGitlabName: "Arushi Sharma",
    gitlabID: "@arushisharma",
    image: arushi,
    role: "Front-end",
    bio: "Hi everyone! I'm a 2nd year CS major from Austin, TX. I like to play golf, hang out with family, go on long walks, and explore new coffee shops.",
    commits: 0,
    issues: 0,
    unitTests: 8,
    emails: ["arushisharma@utexas.edu", "arushisharma264@gmail.com"],
  },
  {
    name: "Nikhita Meka",
    gitlabName: "NikhitaMeka",
    altGitlabName: "Nikhita Meka",
    gitlabID: "@nikhita.meka",
    image: nikhita,
    role: "Front-end",
    bio: "Hi guys! I'm a 2nd year CS major from Austin, TX. In my free time, I enjoy baking and cooking a variety of things, hiking, and interior design.",
    commits: 0,
    issues: 0,
    unitTests: 8,
    emails: [
      "nikhitameka@Nikhitas-MacBook-Pro-2.local",
      "nikhitameka@wireless-10-148-173-239.public.utexas.edu",
      "nikhitameka@wireless-10-145-61-45.public.utexas.edu",
      "nikhitameka@wireless-10-148-221-231.public.utexas.edu",
      "nikhitameka@wireless-10-148-198-252.public.utexas.edu",
      "nikhitameka@wireless-10-154-31-38.public.utexas.edu",
      "nikhitameka@wireless-10-148-243-85.public.utexas.edu",
      "nikhitameka@wireless-10-154-31-38.public.utexas.edu",
    ],
  },
  {
    name: "Anvi Bajpai",
    gitlabName: "Anvi Bajpai",
    altGitlabName: "AnviBajpai",
    gitlabID: "@bajpaianvi1",
    image: anvi,
    role: "Full-stack",
    bio: "Hey y'all, I'm a 2nd year CS major from Plano, TX! In my free time, I like to play badminton, listen to R&B and pop music, and workout.",
    commits: 0,
    issues: 0,
    unitTests: 8,
    emails: ["bajpaianvi1@gmail.com"],
  },
  {
    name: "Ben Matthews",
    gitlabName: "BenMatthews",
    altGitlabName: "Ben Matthews",
    gitlabID: "@benmat8",
    image: ben,
    role: "Back-end",
    bio: "Hi! I'm a 4th year CS major from El Paso, TX. In my free time, I enjoy working out, cooking, and listening to Pink while coding with the group.",
    commits: 0,
    issues: 0,
    unitTests: 8,
    emails: ["bliammatthews@billtrust.com"],
  },
  {
    name: "Jorge Martinez Islas",
    gitlabName: "Jorge Martinez Islas",
    altGitlabName: "JorgeMartinez",
    gitlabID: "@jorgin20026",
    image: jorge,
    role: "Back-end",
    bio: "Hey! I'm a 4th year CS major from Houston, TX. I really like to play video games, watch soccer on TV, and try new restaurants in Austin.",
    commits: 0,
    issues: 0,
    unitTests: 8,
    emails: ["jorgin20026@gmail.com"],
  },
];

// Tool information
const toolInfo = [
  {
    name: "React",
    image: reactlogo,
    description: "Framework for frontend code",
    link: "https://reactjs.org/",
  },
  {
    name: "GitLab",
    image: gitlablogo,
    description: "Git repository and CI/CD platform",
    link: "https://about.gitlab.com/",
  },
  {
    name: "MaterialUI",
    image: materialuilogo,
    description: "Library for React components",
    link: "https://mui.com/",
  },
  {
    name: "NameCheap",
    image: namecheaplogo,
    description: "Service to obtain domain names",
    link: "https://www.namecheap.com/",
  },
  {
    name: "Postman",
    image: postmanlogo,
    description: "API platform for building and using APIs",
    link: "https://www.postman.com/",
  },
  {
    name: "Visual Studio Code",
    image: visualstudiologo,
    description: "Code editor redefined to build and debug applications",
    link: "https://code.visualstudio.com/",
  },
  {
    name: "Python",
    image: pythonlogo,
    description: "A high-level, general-purpose programming language",
    link: "https://www.python.org/",
  },
  {
    name: "Docker",
    image: dockerlogo,
    description: "Service to deliver software in packages called containers",
    link: "https://www.docker.com/",
  },
  
];

export const About = () => {
  // State variables for loading data
  const [loadedCommits, setLoadedC] = useState(false);
  const [loadedIssues, setLoadedI] = useState(false);
  const [teamList, setTeamList] = useState([]);
  const [totalCommits, setTotalCommits] = useState(0);
  const [totalIssues, setTotalIssues] = useState(0);

  // Fetch commit information
  async function getCommitInfo() {
    fetch(`https://gitlab.com/api/v4/projects/54752800/repository/contributors`)
      .then((response) => response.json())
      .then((data) => {
        teamInfo.forEach((member) => {
          member.commits = 0;
        });
        setTotalCommits(0);
        var commits = 0;
        data.forEach((item) => {
          teamInfo.forEach((member) => {
            if (member.emails.find((email) => email === item.email)) {
              member.commits += item.commits;
              commits += item.commits;
            }
          });
        });
        setTotalCommits(commits);
        setTeamList(teamInfo);
        setLoadedC(true);
      });
  }

  // Fetch issues information
  async function getIssuesInfo() {
    fetch(`https://gitlab.com/api/v4/projects/54752800/issues?per_page=100`)
      .then((response) => response.json())
      .then((data) => {
        teamInfo.forEach((member) => {
          member.issues = 0;
        });
        setTotalIssues(0);
        var issues = 0;
        data.forEach((item) => {
          let found = false;
          teamInfo.forEach((member) => {
            if (item.assignee != null) {
              if (
                member.gitlabName === item.assignee.name ||
                member.altGitlabName === item.assignee.name
              ) {
                member.issues += 1;
                issues += 1;
                found = true;
              }
            }
          });
          if (!found) {
            teamInfo.forEach((member) => {
              if (item.closed_by != null) {
                if (member.gitlabName === item.closed_by.name) {
                  member.issues += 1;
                  issues += 1;
                }
              }
            });
          }
        });
        setTotalIssues(issues);
        setTeamList(teamInfo);
        setLoadedI(true);
      });
  }

  // Calculate total tests
  function getTotalTest() {
    var result = 0;
    teamInfo.forEach((member) => {
      result += member.unitTests;
    });
    return result;
  }

  // Fetch data on component mount
  useEffect(() => {
    getIssuesInfo();
    getCommitInfo();
  }, []);

  return (
    <>
      <Box
        sx={{
          backgroundColor: "#f9f9f9",
          padding: "20px",
          borderRadius: "10px",
          marginBottom: "30px",
          maxWidth: "1000px",
          margin: "auto",
          textAlign: "center",
        }}
        data-testid="about-cards"
      >
        <h1
          style={{
            fontFamily: "cursive",
            fontSize: "3rem",
            textShadow: "2px 2px 4px #000",
          }}
        >
          About SafeEats for You
        </h1>
        <Typography variant="h6" style={{ marginTop: "10px", color: "black" }}>
          In the face of the food allergy epidemic, it's more crucial than ever
          to be aware and careful. Our mission is to empower individuals with
          dietary restrictions (Vegetarian, Vegan, Pescatarian, Nut-Free,
          Celiac, Lactose, Soy Allergy, Shellfish, Sesame, etc), ensuring they
          can add variety to their diet without compromising safety. We're
          dedicated to raising awareness about allergies, offering a curated
          platform for discovering safe, certified places to eat, and providing
          personalized recipes and meal plans. Our goal is to enable people to
          confidently enjoy diverse culinary experiences, navigating dining
          choices with ease and joy while prioritizing their well-being. Through
          our project, we aim to build a community where dietary restrictions
          enhance, rather than limit, lifestyle choices.
        </Typography>
      </Box>
      <br />
      <Box>
        {loadedCommits && loadedIssues ? (
          <div>
            <Box>
              <Typography
                variant="h4"
                style={{
                  fontWeight: "bold",
                  textAlign: "center",
                  color: "white",
                }}
              >
                Meet the Team:
              </Typography>
              <Box
                sx={{
                  display: "flex",
                  justifyContent: "center",
                  flexWrap: {
                    xs: "wrap",
                    md: "nowrap",
                  },
                }}
              >
                {teamList.map((member) => (
                  <Box key={member.name}>
                    <Box sx={{ justifySelf: "center" }}>
                      <Card
                        sx={{
                          maxWidth: { xs: "165px", md: "250px" },
                          padding: "10px",
                        }}
                      >
                        <Box sx={{}}>
                          <Box sx={{ justifyContent: "center" }}>
                            <CardMedia
                              component="img"
                              height="300"
                              image={member.image}
                              alt="Profile pic"
                            />
                          </Box>
                          <CardContent sx={{ textAlign: "center" }}>
                            <Typography variant="h6" color="text.secondary">
                              {member.name}
                            </Typography>
                            <Typography variant="h6" color="text.secondary">
                              Role: {member.role}
                            </Typography>
                            <Typography variant="body1">
                              {member.bio}
                            </Typography>
                            <br />
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                              # of commits: {member.commits}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                              # of issues: {member.issues}
                            </Typography>
                            <Typography sx={{ mb: 1.5 }} color="text.secondary">
                              # of unit tests: {member.unitTests}
                            </Typography>
                          </CardContent>
                        </Box>
                      </Card>
                    </Box>
                  </Box>
                ))}
              </Box>
              <Box>
                <br />
                <Box
                  sx={{
                    display: "flex",
                    justifyContent: "space-around",
                    flexWrap: "wrap",
                    paddingBottom: "80px",
                  }}
                >
                  <Typography variant="h5" align="center" color="white">
                    Total Commits: {totalCommits}
                  </Typography>
                  <Typography variant="h5" align="center" color="white">
                    Total Issues: {totalIssues}
                  </Typography>
                  <Typography variant="h5" align="center" color="white">
                    Total Tests: {getTotalTest()}
                  </Typography>
                </Box>
                <Box
                  sx={{
                    backgroundColor: "#636684",
                    padding: "20px",
                    borderRadius: "10px",
                    marginBottom: "30px",
                  }}
                >
                  <Typography
                    variant="h4"
                    sx={{ textAlign: "center", padding: "10px" }}
                  >
                    Our Toolchain:
                  </Typography>
                  <Box
                    sx={{
                      display: "flex",
                      flexWrap: "wrap",
                      justifyContent: "space-around",
                    }}
                  >
                    {toolInfo.map((tool) => {
                      return (
                        <Box key={tool.name}>
                          <Box sx={{ padding: "10px" }}>
                            <Card
                              sx={{
                                width: { xs: "145px", md: "300px" },
                                padding: "10px",
                              }}
                            >
                              <CardMedia
                                component="img"
                                image={tool.image}
                                height="200"
                                sx={{ objectFit: "contain" }}
                              />
                              <CardContent>
                                <Typography variant="h6" color="text.secondary">
                                  {tool.name}
                                </Typography>
                                <Typography
                                  sx={{ mb: 1.5 }}
                                  color="text.secondary"
                                >
                                  {tool.description}
                                </Typography>
                              </CardContent>
                            </Card>
                          </Box>
                        </Box>
                      );
                    })}
                  </Box>
                  <div>
                    <Box>
                      <Typography
                        variant="h6"
                        style={{ fontWeight: "bold", color: "#e5dfda" }}
                      >
                        <br></br>
                        Explanation of the interesting result of integrating
                        disparate data:
                      </Typography>
                      <Typography
                        variant="body1"
                        style={{
                          marginTop: "10px",
                          marginLeft: "100px",
                          marginRight: "100px",
                          color: "#e5dfda",
                        }}
                      >
                        The integration between the various allergies, the
                        specific restaurants, and the unique recipes allows for
                        an integrated information application where those that
                        need to can find all the information they need in one
                        place. One interesting finding from combining different
                        datasets is that when the user is looking for a specific
                        allergy, they will find the information of a recipe and
                        a restaurant that is related to that allergy. This means
                        that these allergies have common data sources that can
                        provide a useful resource to the user.
                      </Typography>
                      <Box
                        sx={{
                          marginTop: "10px",
                          display: "flex",
                          justifyContent: "center",
                        }}
                      >
                        <Card
                          variant="outlined"
                          sx={{
                            margin: "0 10px",
                            padding: "10px",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <Button
                            href="https://documenter.getpostman.com/view/31524688/2sA2r545M5"
                            style={{ width: "100%", height: "100%" }}
                          >
                            <Typography
                              variant="body1"
                              sx={{ textAlign: "center", width: "100%" }}
                            >
                              API Docs
                            </Typography>
                          </Button>
                        </Card>
                        <Card
                          variant="outlined"
                          sx={{
                            margin: "0 10px",
                            padding: "10px",
                            display: "flex",
                            alignItems: "center",
                          }}
                        >
                          <Button
                            href="https://gitlab.com/arushisharma/cs373-group-12"
                            style={{ width: "100%", height: "100%" }}
                          >
                            <Typography
                              variant="body1"
                              sx={{ textAlign: "center", width: "100%" }}
                            >
                              Our Repo
                            </Typography>
                          </Button>
                        </Card>
                      </Box>
                    </Box>
                  </div>
                  <Box
                    sx={{
                      backgroundColor: "white",
                      marginTop: "20px",
                      padding: "10px",
                    }}
                  >
                    <Typography variant="body1" color="black">
                      <b>Data Source:</b>
                      <br />
                      1.{" "}
                      <Button
                        component="a"
                        href="https://docs.developer.yelp.com/docs/fusion-intro"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {" "}
                        Yelp Fusion API (Restful API)
                      </Button>
                      <br />
                      2.{" "}
                      <Button
                        component="a"
                        href="https://developers.google.com/youtube/v3"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Youtube API
                      </Button>
                      <br />
                      3.{" "}
                      <Button
                        component="a"
                        href="https://www.webmd.com/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        WebMD
                      </Button>
                    </Typography>
                  </Box>
                </Box>
              </Box>
            </Box>
          </div>
        ) : (
          <h6>Loading Contributors..</h6>
        )}
      </Box>
    </>
  );
};

export default About;
