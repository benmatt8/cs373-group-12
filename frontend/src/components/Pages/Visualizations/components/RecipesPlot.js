import React, { useState, useEffect } from "react";
import { select, scaleLinear, max, axisBottom, axisLeft } from 'd3';
import Typography from '@mui/material/Typography';

export const RecipesPlot = () => {

  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(175);
  const [numInstances, setMetaData] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const fetchData = async () => {
    let response = await fetch(`${window.backendHost}/recipes?page=${pageNum}&per_page=${perPage}`);

    if (response.ok) {
      setLoaded(true);
      const data = await response.json();
      // setData(data.recipes);
      setMetaData(data.meta.count);
      window.scrollTo(0, 0);

      // Define the dimensions of your SVG
      const margin = { top: 20, right: 20, bottom: 50, left: 50 };
      const width = 960 - margin.left - margin.right;
      const height = 500 - margin.top - margin.bottom;

      // Create an SVG element
      const svg = select('#scatterplot')
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${margin.left}, ${margin.top})`);

      // Define your scales
      const xScale = scaleLinear()
        .domain([0, max(data.recipes, d => d.viewCount)])
        .range([0, width]);

      const yScale = scaleLinear()
        .domain([0, data.recipes.length])
        .range([height, 0]);

      // Create the axes
      const xAxis = axisBottom(xScale).ticks(5);
      const yAxis = axisLeft(yScale).ticks(0);

      svg.append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(xAxis)
        .selectAll('line, path')
        .attr('stroke', 'black');
      svg.select('g')
        .append('text') // append a 'text' element
        .attr('x', width / 2) // position the text in the middle of the axis
        .attr('y', margin.bottom / 2 + 10) // position the text below the axis
        .attr('fill', 'black') // set the fill color to black
        .style('text-anchor', 'middle') // center the text
        .text('View Count'); // set the text
      // .selectAll('line, path')
      // .attr('stroke', 'black');

      svg.append('g')
        .call(yAxis)
        .selectAll('line, path')
        .attr('stroke', 'black');

      const circles = svg.selectAll('circle')
        .data(data.recipes)
        .enter()
        .append('circle')
        .attr('cx', d => xScale(d.viewCount))
        .attr('cy', (d, i) => yScale(i))
        .attr('r', 5)
        .attr('fill', '#434765');

      circles.append('title')
        .text(d => `Recipe: ${d.title}\nView Count: ${d.viewCount}`);
    } else {
      console.error('Error fetching data:', response.status);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  return loaded && (
    <div>
      <Typography variant="h3" style={{ color: "#e5dfda" }}>
        Recipe Views Scatterplot
      </Typography>
      <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
      <div id="scatterplot" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
      <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
    </div>
  );
};

export default RecipesPlot;
