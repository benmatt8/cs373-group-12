import React, { useEffect, useState } from "react";
import { select, pie, arc, scaleOrdinal, schemeCategory10 } from 'd3';
import Typography from '@mui/material/Typography';

export const SubstancesPieChart = () => {
    const [loaded, setLoaded] = useState(false);
    var devApiSubstance = "https://api.substance-abuse.me/substances/count?category"
    var categoryCount = [0, 0, 0]
    var category = ["Stimulant", "Depressant", "Other"]
    const fetchData = async () => {
        for (var i = 0; i < 3; i++) {
            let response = await fetch(`${devApiSubstance}=${category[i]}`);
            if (response.ok) {
                setLoaded(true);
                const data = await response.json()
                categoryCount[i] += data["count"][0]
            }
        }

        const pieData = category.map((cat, i) => ({name:cat, count:categoryCount[i]}))
        const width = 500
        const height = 500
        const radius = Math.min(width, height) / 2
        const svg = select('#pieChart')
            .append('svg')
            .attr('width', width)
            .attr('height', height)
            .append('g')
            .attr('transform', `translate(${width / 2}, ${(height / 2)})`)
        const arcGenerator = arc()
            .innerRadius(0)
            .outerRadius(radius)
        const color = scaleOrdinal(schemeCategory10)
        const pieGenerator = pie().value((d) => d.count)
        svg
            .selectAll('path')
            .data(pieGenerator(pieData))
            .enter()
            .append('path')
            .attr('d', arcGenerator)
            .attr('fill', (d) => color(d.data.name))
            .attr('stroke', 'white')
            .attr('stroke-width', '1px')
            .append('title')
            .text((d) => `${d.data.name}: ${d.data.count}`)
    }
    useEffect(() => {
        fetchData();
      }, []);

    return loaded && (
        <div>
            <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
            <Typography variant="h3" style={{ color: "#e5dfda" }}>
                Substances per Category
            </Typography>
            <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
            <div id="pieChart" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
            <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
        </div>
    );
}

export default SubstancesPieChart