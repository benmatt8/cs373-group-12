import React, { useState, useEffect } from "react";
import { select, pie, arc, scaleOrdinal, schemeCategory10 } from 'd3';
import Typography from '@mui/material/Typography';


export const RestaurantsPieChart = () => {

  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(150);
  const [loaded, setLoaded] = useState(false);
  // const [data, setData] = useState([]);
  const [numInstances, setMetaData] = useState(0);

  const fetchData = async () => {
    let response = await fetch(`${window.backendHost}/restaurants?page=${pageNum}&per_page=${perPage}`);

    if (response.ok) {
      setLoaded(true);
      const data = await response.json();
      // setData(data.restaurants);
      setMetaData(data.meta.count);
      window.scrollTo(0, 0);

      const pieData = data.restaurants.reduce((acc, restaurant) => {
        const allergen = restaurant.allergens;
        if (!acc[allergen]) {
          acc[allergen] = { name: allergen, value: 0 };
        }
        acc[allergen].value++;
        return acc;
      }, {});
      
      const finalPieData = Object.values(pieData);

      const width = 500;
      const height = 500;
      const radius = Math.min(width, height) / 2;

      // setLoaded(true);

      const svg = select('#pieChart')
        .append('svg')
        .attr('width', width)
        .attr('height', height)
        .append('g')
        .attr('transform', `translate(${width / 2}, ${height / 2})`)

      const arcGenerator = arc()
        .innerRadius(0)
        .outerRadius(radius);

      const color = scaleOrdinal(schemeCategory10);

      const pieGenerator = pie().value((d) => d.value);

      svg
        .selectAll('path')
        .data(pieGenerator(finalPieData))
        .enter()
        .append('path')
        .attr('d', arcGenerator)
        .attr('fill', (d) => color(d.data.name))
        .attr('stroke', 'white') // Add a white stroke to separate sections
        .attr('stroke-width', '1px') // Set the stroke width
        .append('title') // Append a title element to each path
        .text((d) => `${d.data.name}: ${d.data.value}`);
        
    } else {
      console.error('Error fetching data:', response.status);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  return loaded && (
    <div>
      <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
      <Typography variant="h3" style={{ color: "#e5dfda" }}>
        Restaurants per Allergy
      </Typography>
      <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
      <div id="pieChart" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
      <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
    </div>
  );
};

export default RestaurantsPieChart;
