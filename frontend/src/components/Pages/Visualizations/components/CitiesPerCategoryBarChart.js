import React, { useEffect, useState } from "react";
import { select, axisLeft, axisBottom, scaleLinear, schemeCategory10, scaleBand, scaleOrdinal } from 'd3';
import Typography from '@mui/material/Typography';

export const CitiesPerCategoryBarChart = () => {
    var devApiCities = "https://api.substance-abuse.me/cities/count?leadingcategory"
    var categoryCount = [0, 0]
    var category = ["Stimulant", "Depressant"]
    const [loaded, setLoaded] = useState(false);
    const fetchData = async () => {
      for (var i = 0; i < 2; i++) {
        let response = await fetch(`${devApiCities}=${category[i]}`);
        if (response.ok) {
          setLoaded(true);
            const data = await response.json()
            categoryCount[i] += data["count"][0]
        }
      }

      const barData = category.map((cat, i) => ({name:cat, count:categoryCount[i]}))
      var margin = {top: 10, right: 30, bottom: 30, left: 60},
          width = 460 - margin.left - margin.right,
          height = 460 - margin.top - margin.bottom;
      var svg = select('#barChart')
          .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
          .append('g')
          .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

      const color = scaleOrdinal(schemeCategory10)
      var x = scaleBand()
        .range([0, width])
        .domain(barData.map(function(d) {return d.name}))
        .padding(0.2);
      svg.append("g")
        .call(axisBottom(x))
        .attr("transform", "translate(0," + height + ")")
        .selectAll("text")
        .attr("transform", "translate(-10,0)")
        .attr("fill", "black")
        .style("text-anchor", "end");
      
      // Add Y axis
      var y = scaleLinear()
        .domain([0, 50])
        .range([ height, 0]);
      svg.append("g")
        .call(axisLeft(y));
      
      // Bars
      const bars = svg.selectAll("mybar")
        .data(barData)
        .enter()
        .append("rect")
          .attr("x", function(d) { return x(d.name); })
          .attr("y", function(d) { return y(d.count); })
          .attr("width", x.bandwidth())
          .attr("height", function(d) { return height - y(d.count); })
          .attr("fill", (d) => color(d.name))
          .text((d) => `${d.name}: ${d.count}`)

          bars.append('title')
          .text(d => `${d.name}\nCount: ${d.count}`);
    }

    useEffect(() => {
        fetchData();
      }, []);

    return loaded && (
      <div>
        <Typography variant="h3" style={{ color: "#e5dfda" }}>
          Number of Cities per Leading Substance Category
        </Typography>
        <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
        <div id="barChart" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
        <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
      </div>
    );
};

export default CitiesPerCategoryBarChart