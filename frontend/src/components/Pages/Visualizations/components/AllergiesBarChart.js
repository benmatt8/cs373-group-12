import React, { useState, useEffect } from "react";
import { scaleBand, scaleLinear, max, axisBottom, axisLeft, select } from 'd3';
import Typography from '@mui/material/Typography';

export const AllergiesBarChart = () => {

  const [pageNum, setPageNum] = useState(1)
  const [perPage, setPerPage] = useState(100);
  const [numInstances, setMetaData] = useState(0);
  const [loaded, setLoaded] = useState(false);

  const fetchData = async () => {
    let response = await fetch(`${window.backendHost}/allergyInfo?page=${pageNum}&per_page=${perPage}`);

    if (response.ok) {
      setLoaded(true);
      const rawData = await response.json();
      let data = rawData.allergyInfo.reduce((acc, allergy) => {
        const severity = allergy.severity;
        if (!acc[severity]) {
          acc[severity] = { severity, count: 0 };
        }
        acc[severity].count++;
        return acc;
      }, {});
      data = Object.values(data);
      setMetaData(rawData.length);
      window.scrollTo(0, 0);

      const margin = { top: 20, right: 20, bottom: 50, left: 50 };
      const width = 960 - margin.left - margin.right;
      const height = 500 - margin.top - margin.bottom;

      // Create the bar chart
      const xScale = scaleBand()
        .domain(data.map(d => d.severity))
        .range([0, width])
        .padding(0.2);
        const yScale = scaleLinear()
        .domain([0, max(data, d => d.count)])
        .range([height, 0])
        .nice();

        const svg = select('#barChart')
        .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
        .append('g')
        .attr('transform', `translate(${margin.left}, ${margin.top})`);

        const bars = svg
        .selectAll('rect')
        .data(data)
        .enter()
        .append('rect')
        .attr('x', d => xScale(d.severity))
        .attr('y', d => yScale(d.count))
        .attr('width', xScale.bandwidth())
        .attr('height', d => height - yScale(d.count))
        .attr('fill', 'steelblue');

        bars.append('title')
        .text(d => `Severity: ${d.severity}\nCount: ${d.count}`);

        svg.append('g')
        .attr('transform', `translate(0, ${height})`)
        .call(axisBottom(xScale))
        .selectAll('line, path')
        .attr('stroke', 'black');
        svg.select('g')
        .append('text') // append a 'text' element
        .attr('x', width / 2) // position the text in the middle of the axis
        .attr('y', margin.bottom / 2) // position the text below the axis
        .attr('fill', 'black') // set the fill color to black
        .style('text-anchor', 'middle') // center the text
        .text('Severity'); // set the text
      
      // For the y-axis:
      svg.append('g')
        .call(axisLeft(yScale))
        .selectAll('line, path')
        .attr('stroke', 'black');
        svg.select('g')
        .append('text') // append a 'text' element
        .attr('transform', 'rotate(-90)') // rotate the text by -90 degrees
        .attr('y', -40) // position the text to the left of the axis
        .attr('x', -height / 2) // position the text in the middle of the axis
        .attr('fill', 'black') // set the fill color to black
        .style('text-anchor', 'middle') // center the text
        .text('Count'); // set the text

    } else {
      console.error('Error fetching data:', response.status);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);


  return loaded && (
    <div>
      <Typography variant="h3" style={{ color: "#e5dfda" }}>
        Allergy Severity Chart
      </Typography>
      <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
      <div id="barChart" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
      <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
    </div>

  );
};

export default AllergiesBarChart;
