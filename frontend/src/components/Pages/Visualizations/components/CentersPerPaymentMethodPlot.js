import React, { useEffect, useState } from "react";
import { select, axisLeft, axisBottom, scaleLinear, scaleOrdinal, scaleBand, schemeCategory10, line } from 'd3';
import Typography from '@mui/material/Typography';

export const CentersPerPaymentMethodPlot = () => {
    var devApiCenters = "https://api.substance-abuse.me/centers/count?payment"
    var categoryCount = [0, 0, 0, 0]
    var category = ["Medicare", "Medicaid", "Military Insurance", "Private Health Insurance"]
    const [loaded, setLoaded] = useState(false);
    const fetchData = async () => {
      for (var i = 0; i < 4; i++) {
        let response = await fetch(`${devApiCenters}=${category[i]}`);
        if (response.ok) {
            setLoaded(true);
            const data = await response.json()
            categoryCount[i] += data["count"]
        }
      }

      var dotData = category.map((cat, i) => ({name:cat, count:categoryCount[i]}))
      const margin = { top: 20, right: 20, bottom: 50, left: 50 };
      const width = 960 - margin.left - margin.right;
      const height = 500 - margin.top - margin.bottom;
      var svg = select('#thirdChart')
          .append('svg')
            .attr('width', width + margin.left + margin.right)
            .attr('height', height + margin.top + margin.bottom)
          .append('g')
          .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

      var x = scaleBand()
        .range([0, width])
        .domain(dotData.map(function(d) {return d.name}))
        .padding(1);
      svg.append("g")
        .call(axisBottom(x))
        .attr("transform", "translate(0," + height + ")")
        .selectAll("text")
        .attr("fill", "black")
        .style("text-anchor", "middle");
      
      var y = scaleLinear()
        .domain([0, 1600])
        .range([ height, 0]);
      svg.append("g")
        .call(axisLeft(y));
      
        svg.append("path")
        .datum(dotData)
        .attr("fill", "none")
        .attr("stroke", "blue")
        .attr("stroke-width", 1.5)
        .attr("d", line()
          .x(function(d) { return x(d.name) })
          .y(function(d) { return y(d.count) })
          )
      // Add the points
      const plots = svg
        .append("g")
        .selectAll("dot")
        .data(dotData)
        .enter()
        .append("circle")
          .attr("cx", function(d) { return x(d.name) } )
          .attr("cy", function(d) { return y(d.count) } )
          .attr("r", 5)
          .attr("fill", "blue")
      
          plots.append('title')
        .text(d => `${d.name}\nCount: ${d.count}`);
    }
    useEffect(() => {
        fetchData();
      }, []);

    return loaded && (
      <div>
        <div>
          <Typography variant="h3" style={{ color: "#e5dfda" }}>
            Number of Centers Taking Specific Payment Methods
          </Typography>
          <hr style={{ borderColor: "#e5dfda", margin: "10px 0" }} />
          <div id="thirdChart" style={{ marginTop: "20px", marginBottom: "40px" }}></div>
          <hr style={{ borderColor: "#e5dfda", margin: "5px 0" }} />
        </div>
      </div>
    );
}

export default CentersPerPaymentMethodPlot