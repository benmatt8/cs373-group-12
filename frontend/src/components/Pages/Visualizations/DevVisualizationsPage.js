import * as React from "react"
import SubstancesPieChart from "./components/SubstancesPieChart";
import CitiesPerCategoryBarChart from "./components/CitiesPerCategoryBarChart";
import CentersPerPaymentMethodPlot from "./components/CentersPerPaymentMethodPlot";

export const DevVisualizationsPage = () => {
    return(
        <div>
            <SubstancesPieChart />
            <CitiesPerCategoryBarChart />
            <CentersPerPaymentMethodPlot />
        </div>
    );
};

export default DevVisualizationsPage