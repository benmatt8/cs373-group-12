import * as React from "react";
import RestaurantsPieChart from "./components/RestaurantsPieChart";
import RecipesPlot from "./components/RecipesPlot";
import AllergiesBarChart from "./components/AllergiesBarChart";

export const VisualizationsPage = () => {
  return(<div>
    {/* <p>Working</p> */}
  <RestaurantsPieChart />
  <RecipesPlot />
  <AllergiesBarChart />
</div>);
};

export default VisualizationsPage;
