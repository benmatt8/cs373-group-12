import React, { useState, useRef } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import "./NavBar.css";
import {HamburgetMenuClose, HamburgetMenuOpen } from "./Icons";
import safeatslogo from "./safe_eats_logo.png";
import forkCursorImage from "./smileyfork.png"; // Import your fork image
import { FaSearch } from "react-icons/fa";


function NavBar() {
  const [click, setClick] = useState(false);
  const [searchQuery, setSearchQuery] = useState(''); // State for search query
  const handleClick = () => setClick(!click);
  const history = useNavigate();


  const handleKeyPress = (event) => {
    if (event.key === 'Enter') {
      handleSearch();
    }
  };


  const inputRef = useRef(null);
  const handleSearch = () => {
    const query = inputRef.current.value.trim();
    setSearchQuery(query);

    if (query !== '') {
      // history(`/allsearch/12`, { state: { query } });
      history(`/allsearch/${query}`);

    }

  };

  return (
    <>
      <nav className="navbar">

      <div className="nav-container">


        
          <NavLink exact to="/" className="nav-logo">
            <img src={safeatslogo} alt="Safe Eats Logo" style={{ width: '175px', height: '100px' }}/>
          </NavLink>


          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <NavLink
                exact
                to="/home"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Home
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/allergyinfo"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Allergy Info
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/restaurants"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Restaurants
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/recipes"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Recipes
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/visualizations"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Visuals
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/devvisualizations"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Provider Visuals
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/critques"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                Critques
              </NavLink>
            </li>
            <li className="nav-item">
              <NavLink
                exact
                to="/about"
                activeClassName="active"
                className="nav-links"
                onClick={handleClick}
                style={{ cursor: `url(${forkCursorImage}), auto` }}
              >
                About
              </NavLink>
            </li>
            {/* change for search bar */}
            <div style={{ display: 'flex', alignItems: 'flex-start', justifyContent: 'center' }}>
  <input
    type="text"
    placeholder="Search..."
    ref={inputRef}
    style={{
      marginBottom: '10px',
      padding: '6px', /* Decreased padding for a smaller size */
      width: '75%', /* Reduced width for a smaller size */
      height: '25px', /* Reduced height for a smaller size */
      borderRadius: '12px', /* Rounded corners for a modern look */
      border: '2px solid #1aa780', /* Changed border color */
      boxShadow: '0 2px 5px rgba(0, 0, 0, 0.1)', /* Added box shadow for depth */
      backgroundColor: '#1f5156' /* Pastel light green color */
    }}
    onKeyPress={handleKeyPress}
  />
  <button
    onClick={handleSearch}
    style={{
      backgroundColor: '#1f5156', /* Changed background color */
      borderRadius: '12px', /* Rounded corners for a modern look */
      border: '2px solid #1aa780', /* Changed border color */
      height: '35px', /* Matched button height with input */
      width: '30px',
      marginLeft: '3px', /* Adjusted margin for alignment */
      padding: '6px', /* Decreased padding for a smaller size */
      borderRadius: '12px', /* Rounded corners for button */
      cursor: 'pointer', /* Cursor pointer for indicating interactivity */
      marginTop: '2.5px' /* Adjusted marginTop to move the button up */
    }}
  >
    <FaSearch color="white" size={12} /> {/* Reduced icon size for a smaller size */}
  </button>
</div>


          </ul>
          
          <div className="nav-icon" onClick={handleClick} style={{ cursor: `url(${forkCursorImage}), auto` }}>
            {/* <i className={click ? "fas fa-times" : "fas fa-bars"}></i> */}

            {click ? (
              <span className="icon">
                <HamburgetMenuOpen />{" "}
              </span>
            ) : (
              <span className="icon">
                <HamburgetMenuClose />
              </span>
            )}
          </div>
        </div>
      </nav>
            {/* Line at the bottom of the navbar */}
            <div className="navbar-line"></div>
    </>
  );
}

export default NavBar;
