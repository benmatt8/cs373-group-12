import "./App.css";
import NavBar from "./components/NavBar";
import { BrowserRouter as Router, Route, Routes, Navigate } from "react-router-dom";
import { Home } from "./components/Pages/Home/Home";
import { AllergyInfo } from "./components/Pages/Models-Info,Restaurants,Recipes/AllergyInfo/AllergyInfo"
import { Restaurants } from "./components/Pages/Models-Info,Restaurants,Recipes/Restaurants/Restaurants";
import { RestaurantDetails } from "./components/Pages/Models-Info,Restaurants,Recipes/Restaurants/Restaurant-Details";
import { Recipes } from "./components/Pages/Models-Info,Restaurants,Recipes/Recipes/Recipes";
import { RecipeDetails } from "./components/Pages/Models-Info,Restaurants,Recipes/Recipes/Recipe-Details";
import { VisualizationsPage } from "./components/Pages/Visualizations/VisualizationsPage";
import { DevVisualizationsPage }from "./components/Pages/Visualizations/DevVisualizationsPage";
import { CritquesPage }from "./components/Pages/Critques/CritquesPage";


import {AllModelSearch} from "./components/Pages/Models-Info,Restaurants,Recipes/AllModelSearch/AllModelSearch"
import { About } from "./components/Pages/AboutUs/About";
import { AllergyInfoDetails } from "./components/Pages/Models-Info,Restaurants,Recipes/AllergyInfo/AllergyInfo-Details";
//http://3.101.109.239
window.backendHost = `https://platform.safeats4you.lol`
function App() {
  return (
    <>
      <Router>
        <NavBar />
        <div className="pages">
          <Routes>
            <Route path="*" element={<Navigate to="/home" />} />
            <Route path="/home" element={<Home />} />
            <Route path="/allergyinfo" element={<AllergyInfo />} />
            <Route path="/allergyinfo/:id" element={<AllergyInfoDetails />} />
            <Route path="/restaurants" element={<Restaurants />} />
            <Route path="/restaurants/:id" element={<RestaurantDetails />} />
            <Route path="/recipes" element={<Recipes />} />
            <Route path="/recipes/:id" element={<RecipeDetails />} />
            <Route path="/about" element={<About />} />
            <Route path="/allsearch/:searchTerm" element={<AllModelSearch />} />
            <Route path="/visualizations" element={<VisualizationsPage />} />
            <Route path="/devvisualizations" element={<DevVisualizationsPage />} />
            <Route path="/critques" element={<CritquesPage />} />
          </Routes>
        </div>
      </Router>
    </>
  );
}

export default App;
